========
Vinstall
========

Vinstall is an application toolkit for the Vector installer, implementing a 
MVCish framework. Currently a work in progress.


Overview
========

Controller
----------

An application written using **vinstall** usually consists in just a set of 
controller classes, implementing a required **interface**. Each controller
class represent a state in the application and they have the following 
responsibilities:

* Defining the next controller class
* Defining the previous controller class 
* Defining the information that will be rendered in the screen
* Reacting to user input

The first two are accomplished by defining the ``next()`` and ``previous()``
methods. The third is accomplished by implementing the ``render()`` method
which needs to return a Render object (see example). The final responsibility
can be implemented by providing a ``process()`` or a ``command()`` method (or
both.) The first one will be executed after user input is processed (usually
after user clicks on "next", this of course depends on how you write your 
views). The last one is scheduled for execution after all the controllers are
processed. A controller class could look like this::

    from vinstall.core.render import Render
    import vinstall.core.model as model

    class MyController(object):
        """A controller
        
        """
        def render(self):
            """A method returning a Render instance.
            The first to args of the Render's constructor are a title
            and an intro text. The rest of the arguments are model objects
            The BooleanOption will be rendered as a checkbox. A few model
            objects representing common form elements are provided by the
            vinstall.core.model module.

            """
            return Render("Hello world", "This is the intro", 
                   model.BooleanOption("This is a boolean option")

        def next(self):
            """Return the next controller class. If this returns None, we 
            assume it is the end of the application.

            """
            return None

        def previous(self):
            """Return the previous controller class.

            """
            return TheFirstController

        def command(self, boolean):
            """The signature matches the number of model objects in the
            render method. This will be executed later.

            """
            if boolean:
               myapp.do_something()


The Application object
----------------------

Finally, you just need to start you app by creating an ``Application``
instance, passing the first controller class (first as in the one representing
the initial state of your app) and the name of a view module as a string.
There are two views defined for the provided model objects, "urwid" and "gtk".
You can start the app using the ``run`` method::

    app = Application("urwid", MyFirstController)
    app.run()

This should be all you need to know for writting a simple app.


Installing
==========

Run ``python setup.py install``. Depending on the view modules you want to use,
you will need to install *urwid* or *gtk* plus the required stuff for the
backend module. Once the dependencies are clarified this will be improved.

