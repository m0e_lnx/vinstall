#-*- coding: utf-8 -*-

"""Automatic install first screen

"""


from vinstall.core import Render, Controller, model, log
from vinstall.backend import (media,  utils,  partitioning, fstab, users,
        service, bootloader, sp, timezone )
import vinstall.controller.intro as intro
import os, glob, shutil


LOG = log.get_logger("automatic")


class AutomaticMode(Controller):
    """Automatic install mode. We will look for devices suitable for
    installation and let the user pick one.

    """
    # Controller interface
    ROOT_FS="ext3"

    def init(self):
        """Initialize some objects

        """
        self.disks = self.find_disks()


    def render(self):
        """Show available disks for user selection

        """
        title = "Target location"
        intro = ("Select a disk for Vector. WARNING: All existing data "
		"in the disk you select will be destroyed.")
        options = [ {"name": str(i)} for i in self.disks ]
        options = model.ExclusiveOptionList(*options)
        

        
        return Render(title, intro, options)

    def process(self, *_):
        """Mount source media so we can read available packages

        """
        self.mount_source()

    def command(self, diskindex):
        """Schedule all the necessary steps for installation.
        Use yield to return a function and a tuple with the arguments needed
        for calling it.

        """
        disk = self.get_disk(diskindex)

        if self.is_live():
            installpkg = self.live_installpkg
            packages = self.live_packages
            install_kernel = self.live_install_kernel
        else:
            installpkg = self.installpkg
            packages = self.packages
            install_kernel = self.install_kernel

        yield self.partitioning, (disk,), "Partitioning %s" % disk
        yield self.format_partitions, (disk,), "Formating partitions"
        yield self.mount_target, (disk,), "Mounting target"

        yield self.preinstall, tuple(), "Preparing package install"

        for package in packages():
            yield installpkg, (package, disk), "Installing %s" % package.split("/")[-1]

        if not self.is_live():
            yield self.postinstall, tuple(), "Installing setup routines"

        yield install_kernel, tuple(), "Installing Linux kernel"

        yield self.set_bind_mounts, tuple(), "Mounting pseudo filesystems"
        yield self.fstab, (disk,), "Creating fstab"
        yield self.setup_services, tuple(), "Setting up system services"
        yield self.vector_version, tuple(), "Writting /etc/vector-version"
        yield self.bootloader, (disk,), "Installing bootloader"
        yield self.clear_bind_mounts, tuple(), "Clearing bind mounts"

    def previous(self):
        """Go back to the first screen

        """
        return intro.Introduction

    def next(self):
        """Jump to the basic Setup

        """
        return UserSetup

    # utility methods

    def find_disks(self):
        """Find the disks available in the system.

        """
        return [ i for i in media.Disk.all() ]

    def get_disk(self, index):
        """Return a disk object from index

        """
        return self.disks[index]

    def partitioning(self, disk):
        """Do automatic partitioning in disk

        """
        available_ram = int(utils.get_mem_size())

        if available_ram <= 512:
            swap_size = 2 * available_ram
        elif available_ram < 1024:
            swap_size = 1024
        else:
            swap_size = int(available_ram / 2)

        disk_size = int(disk.size(unit="MB"))
        partitioner = partitioning.DiskPartitioner(disk)
        if not disk.has_partition_table():
            partitioner.create_partition_table()
        else:
            partitioner.delete_all_partitions()

        root_size = disk_size - swap_size
        partitioner.add_partition(size=root_size, units='MB')
        partitioner.add_partition(size=swap_size-1, units='MB')
        partitioner.write_changes()

    def format_partitions(self, disk):
        """Format partitions in disk with the default filesystem

        """
        proot = "%s%s" % (disk.path(), 1)
        pswap = "%s%s" % (disk.path(), 2)
        utils.umount('/mnt/TARGET')
        utils.format_partition(proot, self.ROOT_FS)
        utils.format_partition(pswap, 'swap')
        utils.activate_swap(pswap)

    def mount_source(self):
        """Mount the install media

        """
        mountpoint = "/mnt/SOURCE"
        if not os.path.exists(mountpoint):
            os.mkdir(mountpoint)
        install_media = self.config["install_media"]
        if install_media.is_iso():
            if not os.path.exists(install_media.path):
                mntpoint = install_media.path.rsplit("/", 1)[0]
                dev = mntpoint.replace("/mnt", "/dev")
                utils.mount(dev, mntpoint)
            utils.mountiso(install_media.path, mountpoint)
        else:
            if install_media.device.is_mounted():
                utils.bind_mount(install_media.device.mountpoint, mountpoint)
            else:
                utils.mount(install_media.path, mountpoint, filesystem="auto")

    def mount_target(self, disk):
        """Mount target partitions

        """
        mountpoint = "/mnt/TARGET"
        if not os.path.exists(mountpoint):
            os.mkdir(mountpoint)
        partition = "%s%s" % (disk.path(), 1)
        utils.mount(partition, mountpoint, filesystem="auto")

    def packages(self):
        """Return a sequence of package objects for installing.
        In automatic mode this should be every package in the iso.
        Ignores anything that starts with "VL_Theme" so it catches themes for any VL flavor.
        """
        excluded =("vlconfig2", "vlsetup", "aaa_base", "shared-mime-info")
        for root, dirs, files in os.walk("/mnt/SOURCE/packages"):
            for file in files:
                fname, fext = os.path.splitext(file)
                if fext in (".txz",".tlz",".tgz"):
                    name = file.rsplit("-", 3)[0]
                    if name in excluded:
                        continue
                    if name.startswith("VL_Theme"):
                        continue
                    yield os.path.join(root, file)

    def installpkg(self, package, disk):
        """Install package in the disk root partition

        """
        command = "/usr/sbin/installpkg %s --root=%s" % (package, "/mnt/TARGET")
        sp.check_call(command.split())

    def preinstall(self):
        """Run before installing packages, used for installing required
        packages first.

        """
        root = "/mnt/TARGET"
        if not self.is_live():
            aaa = glob.glob("/mnt/SOURCE/packages/a/aaa_base-*.t?z")[0]
            self.installpkg(aaa, "/mnt/TARGET")
        else:
            for d in ("tmp", "mnt", "sys", "proc", "dev"):
                d2 = os.path.join(root, d)
                os.mkdir(d2)
            for d in ("cdrom", "cdwriter", "dvd", "dvdwriter", "floppy", "hd",
                    "linux", "loop", "memory", "pendrive", "tmp", "vl-hot",
                    "win", "zip"):
                d2 = os.path.join(root, "mnt", d)
                os.mkdir(d2)

    def postinstall(self):
        """Run after installing packages, use for packages needing to be
        installed at last, or for post install required tasks.

        """
        mimeconf = glob.glob("/mnt/SOURCE/packages/l/shared-mime-info-*.t?z")[0]
        theme = glob.glob("/mnt/SOURCE/packages/xconf/VL_Theme-*.t?z")
        vlconfig = glob.glob("/mnt/SOURCE/packages/a/vlconfig2-*.t?z")[0]
        self.installpkg(mimeconf, "/mnt/TARGET")
        for pkg in theme:
            self.installpkg(pkg, "/mnt/TARGET")
        self.installpkg(vlconfig, "/mnt/TARGET")

    def install_kernel(self):
        """Copy the kernel from the initrd to the target

        kernel is in the ISO as 'sata' in isolinux/kernel/
        Represented in VINSTALL.INI as "sata=version"

        """
        install_media = self.config["install_media"]
        kernel_version = install_media.config.get("kernels", "sata")
        kernel_version = kernel_version.replace("\"", "")
        source = os.path.join('/mnt', 'SOURCE', 'isolinux', 'kernel',
                'sata')
        target = os.path.join('/mnt', 'TARGET', 'boot', 'vmlinuz-%s' %
            kernel_version)
        shutil.copyfile(source, target)

    def is_live(self):
        """Return True if install media is a Live CD

        """
        install_media = self.config["install_media"]
        version = install_media.config.get("general", "version")
        if "LIVE" in version:
            return True
        return False

    def live_packages(self):
        """Return an iterable containing all available packages for a Live
        edition

        """
        return glob.glob("/mnt/SOURCE/*/base/*xzm")

    def live_installpkg(self, package, disk):
        """Install a package from a Live install media (usually a bulk)

        """
        command = "/usr/bin/xzm2dir %s %s" % (package, "/mnt/TARGET")
        sp.check_call(command.split())

    def live_install_kernel(self):
        """Install Linux image from a Live install media

        """
        #XXX do not chdir without switching back to original working dir!
        install_media = self.config["install_media"]
        kernel_version = install_media.config.get("kernels", "sata")
        kernel_version = kernel_version.replace("\"", "")
        source = os.path.join('/mnt', 'SOURCE', 'boot', 'vmlinuz')
        target = os.path.join('/mnt', 'TARGET', 'boot', 'vmlinuz-%s' %
                kernel_version)
        shutil.copyfile(source, target)
        for rc in ("rc.S", "rc.M", "rc.K", "rc.local"):
            rcdir = os.path.join('/mnt', 'TARGET', 'etc', 'rc.d')
            source = os.path.join('%s.real' % rc)
            os.chdir(rcdir)
            os.remove(rc)
            os.symlink(source, rc)

    def fstab(self, disk):
        """Create and install a fstab file in disk

        """
        fstab_obj = fstab.Fstab("/mnt/TARGET/etc/fstab")
        root_entry = fstab.FstabEntry(device = "%s1" % disk.path(),
                mountpoint = "/", filesystem = self.ROOT_FS)
        swap_entry = fstab.FstabEntry(device = "%s2" % disk.path(),
                mountpoint = "none", filesystem="swap")
        for entry in (root_entry, swap_entry):
            fstab_obj.add_entry(entry)
        # Additional entries needed
        proc = fstab.FstabEntry(device="proc",
            mountpoint = "/proc",
            filesystem = "proc",
            options = "defaults 0 0")
        devpts = fstab.FstabEntry(device="none",
            mountpoint = "/dev/pts",
            filesystem = "devpts",
            options = "gid=5,mode=666  0 0"
            )
        sysfs = fstab.FstabEntry(device = "sysfs",
            mountpoint = "/sys",
            filesystem = "sysfs",
            options = "defaults 0 0"
            )
        for item in (proc, devpts, sysfs):
            fstab_obj.add_entry(item)

	# Create the cgroup stuff into fstab
	for g in ("cpuset","cpu","cpuacct","memory","devices",
            "freezer","net_cls","blkio"):
            cgentry = fstab.FstabEntry(device = "cgroup",
                mountpoint = "/cgroup/%s"% g,
                filesystem = "cgroup",
                options = "rw,relatime,%s 0 0" % g,
                )
            fstab_obj.add_entry(cgentry)

    def setup_services(self):
        """Setup default system services

        """
        #XXX:  Define the default set of services for each runlevel
        # At some other location so they can be used by advanced.py too
        svcbasic = [ "cron", "portmap", "lm_sensors", "inetd" ]
        svc2 = svcbasic[:]
        svc3 = svcbasic[:]
        svc3.extend(["samba", "cups", "sshd"])
        svc4 = svcbasic[:]
        svc4.extend(["bluetooth", "fuse", "cups"])
        svc5 = svc3[:]
        svc5.extend(["bluetooth", "fuse"])

        for svc in service.Service.all("/mnt/TARGET"):
            for s, runlevel in ((svc2, 2), (svc3, 3), (svc4, 4), (svc5, 5)):
                if svc.name in s:
                    svc.enable(runlevel)

    def bootloader(self, disk):
        """Install the default bootloader in disk

        """
        grub = bootloader.Grub2(target=disk.path())
        with utils.Chroot("/mnt/TARGET"):
            self.depmod()
            self.mkinitrd(disk)
            grub.backup_config()
            grub.install()
            grub.write_config()

    def mkinitrd(self, disk):
        LOG.debug("Creating initrd")
        try:
            partition = "%s%s" % (disk.path(), 1)
            utils.mkinitrd(partition, self.ROOT_FS)
        except Exception as e:
            # initrd not supported
            LOG.info("Initrd not supported: %s", e)

    def depmod(self):
        LOG.debug("Running depmod -a")
        utils.depmod()

    def install_kernels(self):
        """Copy the kernels from the initrd to the target"""
        src = self.config["install_media"]
        kver = src.config.get("kernels", "sata").replace("\"","")
        ksrc = os.path.join('/mnt', 'SOURCE', 'isolinux','kernel', 'sata')
        ktgt = os.path.join('/mnt', 'TARGET', 'boot', 'vmlinuz-%s' % kver)
        shutil.copyfile(ksrc, ktgt)

    def vector_version(self):
        """write /etc/vector-version based on the information
        in VINSTALL.INI

        """
        install_media = self.config["install_media"]
        distro = install_media.config.get("general", "distro").replace("\"", "")
        version = install_media.config.get("general", "version").replace("\"", "")
        build_date = install_media.config.get("general",
                "build_date").replace("\"", "")
        vector_version =  "%s built on %s\n" % (version, build_date)
        with open("/mnt/TARGET/etc/vector-version", 'w') as f:
            f.write(vector_version)

    def set_bind_mounts(self):
        """Bind mount some paths into the target.
        Later steps will need this when we chroot there

        """
        for point in ("sys","proc","dev"):
            cmd = "mount -o bind /%s /mnt/TARGET/%s" % (point, point)
            sp.check_call(cmd.split())

    def clear_bind_mounts(self):
        """Clear the mountpoints that were mounted for the configuration

        """
        for point in ("sys","proc","dev"):
            cmd = "umount -f /mnt/TARGET/%s" % point
            sp.check_call(cmd.split())


class UserSetup(object):
    """Minimal configuration - Ask for username and passwords

    """
    def __init__(self):
	self.timezones = timezone.timezones()
        self.hw_clock_options = ["localtime", "UTC"]
        
    def render(self):
        """Ask for username and passwords

        """
        username = model.TextOption("Username")
        password = model.TextOption("Password")
        rootpassword = model.PasswordOption("Admin password")
        title = "Setup users and passwords."
        intro = "Setup your user account and administrator password"
        
        tz_title = model.SimpleText("Time settings")

        tz_options = model.DropdownOptionList()
        tz_options.label = u"Select your timezone"
        for i in self.timezones:
            tz_options.add_option(i)

        clock_options = model.DropdownOptionList()
        clock_options.label = u"My hardware clock is set to"
        for i in self.hw_clock_options:
            clock_options.add_option(i)

	#XX: Should we even offer hwclock option in automatic mode?
        return Render(title, intro, username, password, rootpassword,
			tz_title, tz_options, clock_options)

    def next(self):
        """Return next step

        """
        return None

    def previous(self):
        """Return previous step

        """
        return AutomaticMode

    def command(self, username, password, rootpassword, tz, hwclock):
        """Create a user and setup passwords

        """
        if rootpassword.strip():
            yield self.set_root_password, (rootpassword,), "Creating users"
        if username.strip() and password.strip():
            yield self.create_user_account, (username, password), \
                    "Setting passwords"
        yield timezone.set_timezone, (self.timezones[tz], 
			"/mnt/TARGET"), "Setting timezone"
        yield timezone.set_hw_clock, (self.hw_clock_options[hwclock],
            "/mnt/TARGET"), "Setting hardware clock"

    def set_root_password(self, rootpassword):
        """Set the root password

        """
        admin = users.User()
        admin.root = "/mnt/TARGET"
        admin.login = "root"
        with utils.Chroot("/mnt/TARGET"):
            admin.change_password(rootpassword)

    def create_user_account(self, username, userpassword):
        """Create a user account and set its password

        """
        account = users.User()
        account.root = "/mnt/TARGET"
        account.login = username
        account.password = userpassword
        with utils.Chroot("/mnt/TARGET"):
            account.create()
