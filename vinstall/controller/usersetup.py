#-*- coding: utf-8 -*-

"""User setup

"""


from vinstall.core import Render, Controller, model
from vinstall.backend import users, utils, fstab, service, timezone


class UserSetup(Controller):
    """Setup your user and passwords

    """
    def init(self):
        self.timezones = timezone.timezones()
        self.hw_clock_options = ["localtime", "UTC"]

    def render(self):
        """Ask for username and passwords

        """
        title = u"Users setup and system time configuration"
        intro = u"Setup your user account and set the initial system time configuration"

        username = model.TextOption("Username")
        password = model.TextOption("Password")
        rootpassword = model.PasswordOption("Admin password")

        tz_title = model.SimpleText("Time settings")

        tz_options = model.DropdownOptionList()
        tz_options.label = u"Select your timezone"
        for i in self.timezones:
            tz_options.add_option(i)

        clock_options = model.DropdownOptionList()
        clock_options.label = u"My hardware clock is set to"
        for i in self.hw_clock_options:
            clock_options.add_option(i)

        return Render(title, intro, username, password, rootpassword,
                tz_title, tz_options, clock_options)

    def next(self):
        """Return next step

        """
        from vinstall.controller import bootloader
        return bootloader.Bootloader

    def previous(self):
        """Return previous step

        """
        from vinstall.controller import packages
        return packages.PackageSelection

    def command(self, username, password, rootpassword, tz, hwclock):
        """Create a user and setup passwords

        """
        if rootpassword.strip():
            yield self.set_root_password, (rootpassword,), "Creating users"
        if username.strip() and password.strip():
            yield self.create_user_account, (username, password), "Setting passwords"
        yield self.fstab, tuple(), "Creating fstab"
        yield self.setup_services, tuple(), "Setting up services"
        yield timezone.set_timezone, (self.timezones[tz], "/mnt/TARGET"), "Setting timezone"
        yield timezone.set_hw_clock, (self.hw_clock_options[hwclock],
                "/mnt/TARGET"), "Setting hardware clock"

    def set_root_password(self, rootpassword):
        """Set the root password

        """
        admin = users.User()
        admin.root = "/mnt/TARGET"
        admin.login = "root"
        with utils.Chroot("/mnt/TARGET"):
            admin.change_password(rootpassword)

    def create_user_account(self, username, userpassword):
        """Create a user account and set its password

        """
        account = users.User()
        account.root = "/mnt/TARGET"
        account.login = username
        account.password = userpassword
        with utils.Chroot("/mnt/TARGET"):
            account.create()

    def fstab(self):
        """Create and install a fstab file in disk

        """
        fstab_obj = fstab.Fstab("/mnt/TARGET/etc/fstab")

        mountpoints = self.config.get("mountpoints")
        for dev, mount, fs in mountpoints:
            if not fs:
                fs = dev.query_filesystem()

            entry = fstab.FstabEntry(
                    device=dev.device_path,
                    mountpoint=mount,
                    filesystem=fs)
            fstab_obj.add_entry(entry)
        # Additional entries needed
        proc = fstab.FstabEntry(device="proc",
            mountpoint = "/proc",
            filesystem = "proc",
            options = "defaults 0 0")
        devpts = fstab.FstabEntry(device="none",
            mountpoint = "/dev/pts",
            filesystem = "devpts",
            options = "gid=5,mode=666  0 0"
            )
        sysfs = fstab.FstabEntry(device = "sysfs",
            mountpoint = "/sys",
            filesystem = "sysfs",
            options = "defaults 0 0"
            )
        for item in (proc, devpts, sysfs):
            fstab_obj.add_entry(item)

	# Create the cgroup stuff into fstab
	for g in ("cpuset","cpu","cpuacct","memory","devices",
            "freezer","net_cls","blkio"):
            cgentry = fstab.FstabEntry(device = "cgroup",
                mountpoint = "/cgroup/%s"% g,
                filesystem = "cgroup",
                options = "rw,relatime,%s 0 0" % g,
                )
            fstab_obj.add_entry(cgentry)

    def setup_services(self):
        """Setup default system services

        """
        svcbasic = [ "cron", "portmap", "lm_sensors", "inetd" ]
        svc2 = svcbasic[:]
        svc3 = svcbasic[:]
        svc3.extend(["samba", "cups", "sshd"])
        svc4 = svcbasic[:]
        svc4.extend(["bluetooth", "fuse", "cups"])
        svc5 = svc3[:]
        svc5.extend(["bluetooth", "fuse"])

        for svc in service.Service.all("/mnt/TARGET"):
            for s, runlevel in ((svc2, 2), (svc3, 3), (svc4, 4), (svc5, 5)):
                if svc.name in s:
                    svc.enable(runlevel)

