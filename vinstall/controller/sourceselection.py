#-*- coding: utf-8 -*-

"""The VectorLinux installer

"""

from vinstall.core import Render, Controller, model
from vinstall.controller import intro


class SourceSelection(Controller):
    """Select a install media source to install from

    """
    def render(self):
        """Present the install types to the user

        """
        title = "Installation Source Media"
        intro = ("Select a source to install Vector from:")
        sources = [ {"name": "%s" % str(i)} for i in self.config["sources"] ]
        options = model.ExclusiveOptionList(*sources)
        return Render(title, intro, options)

    def next(self):
        """Return next controller depending on self._next.
        self._next will be defined by user selection

        """
        return intro.Introduction

    def previous(self):
        """This is the first one, return None

        """
        return None

    def process(self, index):
        """Set the install_media key to the proper value in the config

        """
        self.config["install_media"] = self.config["sources"][index]

