#-*- coding: utf-8 -*-

"""Partitions options for the Vector installer

"""


from vinstall.core import Render, Controller, model
from vinstall.backend import media, sp
import os
import subprocess


class AdvancedMode(Controller):
    """First stage of the advanced mode.
    Ask to the user if she wants to use existing partitions or run a
    partitioning program for preparing partitions

    """
    def init(self):
        self.selection = None

    def render(self):
        """Show partitioning options

        """
        title = "Disk Paritioning"
        intro = ("You can create new partitions for your installation or use"
                " existing partitions in your system.")
        options = model.ExclusiveOptionList()
        options.append({"name": "I will use existing partitions"})
        options.append({"name": "Launch a partitioning program"})
        return Render(title, intro, options)

    def next(self):
        """Return next controller

        """
        if self.is_display_set() or self.selection == 0:
            from vinstall.controller import mountpoints
            return mountpoints.Mountpoint
        else:
            return TUIPartitionManager

    def previous(self):
        """Return previous step

        """
        from vinstall.controller import intro
        return intro.Introduction

    def process(self, index):
        """Launch partitioning program if needed

        """
        self.selection = index
        if index == 1:
            if self.is_display_set():
                command = "/usr/sbin/gparted".split()
                sp.call(command)

    def is_display_set(self):
        """Return True if graphical program should be used

        """
        if os.environ.get("DISPLAY"):
            return True
        return False



class TUIPartitionManager(Controller):
    """Launch a text based partition manager.
    Ask user which disk wants to create partitions on.

    """

    def init(self):
        """Find available disks

        """
        self.disks = list(media.Disk.all())

    def render(self):
        """disk selection

        """
        dropdown = model.DropdownOptionList(label="Select disk")
        for disk in self.disks:
            dropdown.add_option(str(disk))
        return Render("Select disk", ("Select a disk for partitioning "
            "with cfdisk. When you finish you will have the chance to "
            "pick another disk to do more partitions or move on with "
            "the next step."), dropdown)

    def next(self):
        """Next controller

        """
        return PostPartitioning

    def previous(self):
        """Previous controller

        """
        return AdvancedMode

    def process(self, disk):
        """Launch cfdisk

        """
        # do not use our subprocess wrapper, we need stdout
        try:
            cmd = "/sbin/cfdisk %s" % self.disks[disk].path()
            subprocess.check_call(cmd.split())
        except subprocess.CalledProcessError as error:
            if error.returncode == 4:
                cmd = "/sbin/cfdisk -z %s" % self.disks[disk].path()
                subprocess.check_call(cmd.split())
            else:
                raise


class PostPartitioning(Controller):
    """Ask if we should cfdisk another disk or move on

    """
    def init(self):
        self.selection = None

    def render(self):
        title = "Disk Partitioning"
        intro = ""
        options = model.ExclusiveOptionList(
                {"name": "I am done with partitions, move on"},
                {"name": "I want to select another disk"})
        return Render(title, intro, options)

    def process(self, index):
        "Expose the selection"
        self.selection = index

    def next(self):
        """Next controller

        """
        if self.selection == 0:
            from vinstall.controller import mountpoints
            return mountpoints.Mountpoint
        else:
            return TUIPartitionManager

    def previous(self):
        """Previous controller

        """
        return TUIPartitionManager
