#-*- coding: utf-8 -*-

"""Bootloader options for the Vector installer

"""


import os, shutil
from vinstall.backend import bootloader, utils, sp
from vinstall.backend.media import Disk, Partition
from vinstall.core import Render, Controller, model, log


LOG = log.get_logger("bootloader_controller")


class Bootloader(Controller):
    """Select a bootloader

    """
    def init(self):
        """Find the disks available in the system.

        """
        self.disks = [ i for i in Disk.all() if not i.is_read_only() ]
        self.partitions = [ i for i in Partition.all() if i.query_filesystem()
                != "xfs" ]
        self.targets = self.disks + self.partitions
        self.bootloaders = [ u"Grub2", u"Lilo", u"None" ]

    def render(self):
        """Show bootloader options

        """
        title = u"Bootloader"
        intro = u"Select a bootloader to boot your system"

        bootloaders = model.DropdownOptionList()
        bootloaders.label = u"Select a bootloader"
        for i in self.bootloaders:
            bootloaders.add_option(i)

        location = model.DropdownOptionList()
        location.label = u"Install the bootloader in"
	splash_opt = model.BoolOption(u"Disable boot animation","Disable boot animation","Disable bootsplash")
        for i in self.targets:
            location.add_option(unicode(i))

	return Render(title, intro, bootloaders, location)
#        return Render(title, intro, bootloaders, location, splash_opt)
#XX:	^^ Do not render the splash_opt anymore... this is no longer supported.

    def next(self):
        """Return next step

        """
        return None

    def previous(self):
        """Return previous step

        """
        from vinstall.controller import usersetup
        return usersetup.UserSetup


    def command(self, bootloader, target, splash_opt = False):
        """Schedule command for later execution

        """
        # splash_opt is TRUE if the user selects to disable bootsplash.
        LOG.debug("Bootloader: %s", bootloader)
        LOG.debug("Target: %s", target)
        yield self.set_bind_mounts, tuple(), "Mounting pseudo filesystems"
        yield self.depmod, tuple(), "Running depmod"
        yield self.mkinitrd, (splash_opt,), "Creating initrd"
        yield self.install_bootloader, (bootloader, target, splash_opt), "Installing bootloader"
        yield self.clear_bind_mounts, tuple(), "Clearing bind mounts"
	yield self.save_installation_logs, tuple(), "Saving installation logs"

    def save_installation_logs(self):
	"""Save the installation logs to the target"""
	#logloc = os.getcwd()
        logloc = os.environ["CWD"]
	logtarget = "/mnt/TARGET/var/log"
	logfiles=(('installer.log','installer.log'),
		  ('subprocess.log','installer-subprocess.log'))
	for x in logfiles:
	    logs = os.path.join(logloc, x[0])
	    logt = os.path.join(logtarget, x[1])
	    LOG.debug("Copying %s to %s" %(logs, logt))
	    try:
		os.makedirs(logtarget)
	    except:
		pass
	    shutil.copy(logs, logt)

    def install_bootloader(self, bloader, target, splash_opt):
        """Install the bootloader

        """
        bloader = self.bootloaders[bloader]
        disk = self.targets[target]
        target = disk.path()
	swaps = [ p[0] for p in self.config["mountpoints"] if p[1] == "swap" ]
	swap_path = None
	if swaps:
	    swap_dev = swaps[0]
	    swap_path = swap_dev.path()

        if bloader.lower() == "lilo":
            self.install_lilo(target=target, splash_opt=splash_opt,
			    swap_dev=swap_path)
        elif bloader == "Grub2":
            self.install_grub(target=target, splash_disabled=splash_opt,
			    )
        else:
            return

    def install_lilo(self, target, splash_opt=False, swap_dev=None):
        """ Install LILO to target

        """
        LOG.debug("Installing LILO to %s", target)
        # Set the current root.  We would normally use utils.get_mounted("/") for this
        # but on a new install, that returns None, so we must give it the
        # self.config['target_device'] object itself.
        #
        # splash_opt value of False means splash was disabled by the user
	with utils.Chroot("/mnt/TARGET"):
            current_root = self.config['target_device'].path()
	    lilo = bootloader.Lilo(target = target,
		                   bootloader_root = current_root, 
				   )

            for system in bootloader.OperatingSystem.all(
                    include_running_os = True,
                    current_root = self.config['target_device']):
                if system.root == current_root:
                    if splash_opt:
			    system.appendline = "verbose resume=%s"% swap
                    else:
                        system.appendline = "quiet splash"
		    if swap_dev:
			system.appendline = "%s resume=%s" % (system.appendline, swap_dev)

		lilo.add_os(system)
		LOG.debug("Adding LiLO boot menu item for %s from %s"% \
			(system.label, system.root))
		if system.root == current_root and "-TUI" not in system.label:
		    LOG.debug("Setting %s from %s as the default boot OS"% \
			(system.label, system.root))
		    lilo.set_default_os(system)

	    lilo.backup_config()
	    lilo.write_config()
	    lilo.install()

    def install_grub(self, target, splash_disabled=False):
        """ Install Grub2 to target

        """
        LOG.debug("Installing Grub2 to %s", target)
        grub2 = bootloader.Grub2(target)
	swaps = [ p[0] for p in self.config["mountpoints"] if p[1] == "swap" ]
	swap_path = None
	if swaps:
	    swap_dev = swaps[0]
	    swap_path = swap_dev.path()

        with utils.Chroot("/mnt/TARGET"):
            grub2.backup_config()
            grub2.install()
            grub2.write_config(splash_disabled = splash_disabled,
			    swap_path=swap_path)

    def mkinitrd(self, splash_disabled=False):

        LOG.debug("Creating initrd")
        device = self.config["target_device"]
        fs = self.config["target_device_fs"]
        err = None
        with utils.Chroot("/mnt/TARGET"):
            try:
                utils.mkinitrd(device.path(), fs, splash_disabled)
            except Exception as e:
                # initrd no supported
                err = e
        if err is not None:
            LOG.error("Initrd not supported: %s", e)

    def depmod(self):
        LOG.debug("Running depmod -a")
        with utils.Chroot("/mnt/TARGET"):
            utils.depmod()

    def clear_bind_mounts(self):
        """Clear the mountpoints that were mounted for the configuration

        """
        for mpoint in ("sys","proc","dev"):
            cmd = "umount -f /mnt/TARGET/%s" % mpoint
            sp.check_call(cmd.split())

    def set_bind_mounts(self):
        """Bind mount some paths into the target.
        Later steps will need this when we chroot there

        """
        LOG.debug("Bind mounting filesystems")
        for point in ("sys","proc","dev"):
            cmd = "mount -o bind /%s /mnt/TARGET/%s" % (point, point)
            sp.check_call(cmd.split())
