# coding: utf8


"""Small subprocess wrapper


"""


import subprocess
import functools
from vinstall.core import log


LOG = log.get_logger(__name__)

__all__ = ["call", "check_call", "check_output"]


_LOGFILE = "subprocess.log"


def make_sp_wrapper(func):
    def wrapper(*args, **kwargs):
        with open(_LOGFILE, 'a') as stdio:
            if 'stdout' not in kwargs:
                kwargs['stdout'] = stdio
            if 'stderr' not in kwargs:
                kwargs['stderr'] = stdio
            return func(*args, **kwargs)
    return wrapper


call = make_sp_wrapper(subprocess.call)
check_call = make_sp_wrapper(subprocess.check_call)
try:
    check_output = subprocess.check_output
except subprocess.CalledProcessError as err:
    LOG.error("Subprocessed command returned error")
    LOG.error(err.output)

