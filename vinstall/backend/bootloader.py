# coding: utf8


"""Bootloader setup

>>> lilo = Lilo("/dev/sda1") # pass main os root
>>> lilo.backup_config()
... for system in OperatingSystem.all():
...     lilo.add_os(system)
>>> lilo.write_config()
>>> lilo.install()

"""


__author__ = "M0E-lnx"


import os
import shutil
import tempfile
from StringIO import StringIO
from vinstall.backend import utils, media
from vinstall.backend import sp
import unittest
from vinstall.core import log

LILO = object()
GRUB = object()
GRUB2 = object()

LOG = log.get_logger("bootloader_backend")

class LiloDotConfEntry(object):
    """Dummy wrapper for reading a lilo.conf.  Has absolutely no use at all.
    The only reason it exists is to do some unittesting"""
    root = None
    label = None

    @classmethod
    def all(cls, fpath):
	entries = []
	with open(fpath) as data:
	    for line in data:
		line = line.strip()
		if line.startswith("root") or \
		  line.startswith("other"):
		    entry = cls()
		    entry.root = line.split("=")[-1].strip()
		    yield entry

class Lilo(object):
    """LiLo bootloader

    """
    def __init__(self, target, default_os=None, timeout=3,
            vga_mode="788", bootloader_root=None,
	    ):
	"""
	arguments:
		target: target device or partition where lilo will be installed
		default_os: default os to be booted after the timeout value if the user does not take action
		timeout: N in seconds the boot menu should be displayed
		vga_mode: vga mode used for boot menu and while booting
		bootloader_root: Path to root partition containing the bootloader config
		
	"""

        self.target = target
        self.default_os = default_os
        self.vga_mode = vga_mode
        self.operating_systems = []
        self.timeout = None
        self.set_timeout(timeout)
        self.buffer = []
	if not bootloader_root:
            self.lilo_root = utils.get_mounted("/")
	else:
	    self.lilo_root = bootloader_root
	self.bootloader_root = bootloader_root
        self.tamu = "/boot/tamu"
        if not os.path.exists(self.tamu):
            os.mkdir(self.tamu)


    def add_os(self, system):
	"""Add the information about this operating system to the
	class"""
        if system.type == "linux":
            self.add_unix_os(system)
        elif "chain" in system.type:
            self.add_ms_os(system)
        else:
            #XXX
            pass

    def copy_unix_bootdata(self, system):
	"""Copy the kernel and initrd to our partition
	if necessary"""
	
	umount = False
        if system.type != "linux":
	    return
	if system.os_root != self.bootloader_root:
	    # Mount the os root partition if needed
	    partition = media.Partition(system.root)
	    if not partition.is_mounted():
		umount = True
		if not os.path.exists(partition.mountpoint):
		    os.mkdir(partition.mountpoint)
		mountpoint = partition.mount(partition.mountpoint)
	    else:
		mountpoint = partition.mountpoint

	    # Copy the kernel to the local tamu path
	    kernel_from = mountpoint + system.kernel #XX: os.path.join wont work here!!
	    kernel_to = system.tamu_kernel_path

	    if not os.path.exists(os.path.split(kernel_to)[0]):
	        os.mkdir(os.path.split(kernel_to)[0])
	    shutil.copy2(kernel_from, kernel_to)

	    # Copy the initrd to the local tamu path
	    if system.initrd:
	        initrd_from = mountpoint + system.initrd #XX: os.path.join wont work here !!
		if not os.path.exists(initrd_from):
		    LOG.debug("WARNING:  reported initrd for %s (%s) does not exist"% (
			system.label, system.initrd))
		else:
		    shutil.copy2(initrd_from, system.tamu_initrd_path)

        # un-mount the partition if necessary
	if umount:
	    partition.umount()

    def add_unix_os(self, system):
        """Add an operating system to the boot menu

        """
        if system in self.operating_systems:
            raise RuntimeError("OS already added")

        # write config lines
        tab = " " * 4
        os_host = None
        if not isinstance(system.root, media.Partition):
            for p in media.Partition.all():
                if p.path() == system.root:
                    os_host = p
                    break
        
        if os_host is None:
            LOG.error("Unable to find partition object for %s"% system.root)
            return

        if not os_host.uuid or os_host.uuid == "None":
            os_rootpath = os_host.path()
        else:
            os_rootpath = "\"UUID=%s\""% os_host.uuid
#            os_rootpath = "/dev/disk/by-uuid/%s"% os_host.uuid
        #XX:  ^^ Prefer UUID when available

        self.buffer.append("# -- %s on %s --" % (system.label,
            system.root))
	
	if system.root != self.bootloader_root:
	    self.buffer.append("image = %s"% system.tamu_kernel_path)
	    self.buffer.append("%s label = %s"% (tab, system.tamu_lilo_label))
	    if system.initrd:
	        self.buffer.append("%s initrd = %s"% (tab, system.tamu_initrd_path))
	else:
	    self.buffer.append("image = %s"% system.kernel)
	    self.buffer.append("%s label = %s"% (tab, system.label))
            if "-TUI" in system.label:
                system.appendline = "2 verbose"
	    if system.initrd:
		self.buffer.append("%s initrd = %s"% ( tab, system.initrd))
	self.buffer.append("%s root = %s" % (tab, os_rootpath))
        #XX: ^^ This will be a path by UUID where available.  Device path when not.
        if system.appendline:
	    self.buffer.append('%s append = "%s"'% (tab, system.appendline))
            
        self.buffer.append("%s read-only" % tab)
        self.buffer.append("# --")

        self.operating_systems.append(system)

    def add_ms_os(self, system):
        """Add MS os to boot menu

        """
	tab = " " * 4
        self.buffer.append("# -- %s on %s --" % (system.long_desc, system.root))
        self.buffer.append("other = %s" % system.root)
        self.buffer.append("%s label = %s" % (tab, system.label))
        self.buffer.append("%s table = %s" % (tab, system.root[:-1]))
	self.operating_systems.append(system)
        
    def set_default_os(self, system):
        """Set the default OS

        """
        self.default_os = system

    def set_timeout(self, seconds):
        """Wait for <timeout> seconds before booting the default OS

        """
        self.timeout = int(seconds) * 10

    def set_vga_mode(self, resolution, quality):
        """Set the VGA mode

        """
        values = {
                "640x480" : {
                    "low" : "769",
                    "med" : "784",
                    "high" : "785"},
                "800x600" : {
                    "low" : "771",
                    "med" : "787",
                    "high" : "788"},
                "1024x768" : {
                    "low" : "773",
                    "med" : "790",
                    "high" : "791"},
                "1280x1024": {
                    "high":"794",
                    "med":"793",
                    "low":"775"}}

        self.vga_mode = values[resolution][quality]

    def backup_config(self):
        """Backup existing lilo.conf

        """

    def write_config(self, config_path="/etc/lilo.conf"):
        """Write lilo.conf.

        """
        newline = "\n"
        bootloader_target = None
        for d in media.Disk.all():
            if d.path() == self.target:
                LOG.debug("Identified bootloader target as MBR on %s"% d)
                bootloader_target = d
                if d.id is None:
                    target_line = self.target
                else:
                    target_line = os.path.join("/dev/disk/by-id", d.id)
                break
        if bootloader_target is None:
            for p in media.Partition.all():
                if p.path() == self.target:
                    LOG.debug("Identified bootloader target as BOOTSECTOR of %s"% p)
                    bootloader_target = p
                    target_line = os.path.join('/dev/disk/by-uuid', p.uuid)
                    break

        if bootloader_target is None:
            # Give up... we dont know what was chosen.
            LOG.error("Unable to determine if bootloader target is a Disk or Partition")
            raise NotImplemented

	if self.default_os:
	    default_os_line = "default = %s"% self.default_os.label
	else:
	    default_os_line = "#"
        if os.path.exists("/boot/bitmap/boot.bmp"):
            bitmap_line = "bitmap = /boot/bitmap/boot.bmp"
        else:
            bitmap_line = "# bitmap = /boot/bitmap/boot.bmp"

        header = (
            "# LILO configuration file.",
            "# ",
            "# Generated by VASM.",
            "# ",
            "boot = %s" % target_line,
	    default_os_line,
            bitmap_line,
            "compact",
            "prompt",
            "timeout = %s" % self.timeout,
            "change-rules",
            "reset",
            "vga = 788",
            "# OS list")

        with open(config_path, "w") as liloconf:
            for i in header:
                liloconf.write(i + newline)
            for line in self.buffer:
                liloconf.write(line + newline)

    def install(self):
        """Install the bootloader

        """
	for ops in self.operating_systems:
            self.copy_unix_bootdata(ops)
        return sp.check_call(["/sbin/lilo"])

    def test_install(self, config_file, bootdev):
        """Install routine for testing

        """
        cmd = "/sbin/lilo -b %s -C %s" % (bootdev, config_file)
        return sp.check_call(cmd.split())


class Grub2(object):
    """Grub2 bootloader

    """
    def __init__(self, target):
        self.config_path = "/etc/default/grub"
        self.type = GRUB2
        self.timeout = 5
        self.target = target
        self.vga_mode = None
        self.set_vga_mode()

    def set_vga_mode(self, resolution="800x600"):
        modes = {
                "640x480": "640x480x16",
                "800x600": "800x600x16",
                "1024x768": "1024x768x16",
                "1280x1024": "1280x1024x16"}
        self.vga_mode = modes.get(resolution, "800x600")

    def backup_config(self):
        pass

    def write_config(self, config_path="/etc/default/grub",
                     splash_disabled=False, swap_path=None):
        """The grub2 config file is automatically generated, so we use
        this method to save the values to /etc/default/grub which is
        parsed while the config file is generated

        """

        # set the correct vga_mode value
        if not self.vga_mode:
            self.set_vga_mode()
        
        if splash_disabled:
            splash_toggle="NO"
        else:
            splash_toggle="YES"

	if os.path.exists("/usr/lib64"):
	    grubdist="VLocity"
	else:
	    grubdist="Vector"

	if swap_path:
	    cmdline_linux="GRUB_CMDLINE_LINUX=\"resume=%s\"" % swap_path
	else:
	    cmdline_linux=""

        ndata = ["export GRUB_GFXMODE=%s"% self.vga_mode,
		 "export GRUB_DISTRIBUTOR=\"%s Linux $(cat /etc/vector-version | cut -f 1-2 -d \' \')\""% grubdist,
                 "export GRUB_TERMINAL=gfxterm",
		 "export GRUB_TERMINAL_OUTPUT=gfxterm",
                 "export GRUB_DISABLE_LINUX_UUID=false",
		 "export GRUB_FONT=/usr/share/grub/unifont.pf2",
                 "export GRUB_TIMEOUT=%s"% self.timeout,
		 "GRUB_USE_BOOTSPLASH=\"%s\""% splash_toggle,
		 cmdline_linux,
                 " ",
                 "### DO NOT EDIT BELOW THIS LINE ###",
                 "if [ \"$GRUB_USE_BOOTSPLASH\" != \"YES\" ]; then",
                 "    export GRUB_BOOTSPLASH_OPT=\"verbose\"",
                 "else",
                 "    export GRUB_BOOTSPLASH_OPT=\"quiet splash\"",
                 "fi",
                 " ",
                 "if [ \"$GRUB_GFXMODE\" = \"1024x768x16\" ]; then",
                 "    VGA=791 ",
                 "elif [ \"$GRUB_GFXMODE\" = \"800x600x16\" ]; then",
                 "    VGA=788 ",
                 "elif [ \"$GRUB_GFXMODE\" = \"1280x1024x16\" ]; then",
                 "    VGA=794 ",
                 "else",
                 "    VGA=normal",
                 "fi",
                 "# END OF /etc/default/grub",""]

        with open(config_path, "w") as f:
            f.writelines("\n".join(ndata))

        # after these are set, we just need to run grub-mkconfig
        cfg = "/boot/grub/grub.cfg"
        new = cfg + ".new"
        sp.check_call(["grub-mkconfig", "-o", cfg])
        if os.path.exists(new):
            shutil.move(new, cfg)

    def install(self):
        """Install grub to the target

        """
        if os.path.exists("/usr/sbin/grub-mkdevicemap"):
            sp.check_call(["grub-mkdevicemap", "--no-floppy"])
        sp.check_call(["grub-install", "--force","--no-floppy", self.target])

    def test_install(self, config_file, bootdev):
        if os.path.exists("/usr/sbin/grub-mkdevicemap"):
            sp.check_call(["grub-mkdevicemap", "--no-floppy"])
        sp.check_call(["grub-install", "--no-floppy", bootdev])


class OperatingSystem(object):
    """An OS to be added to a bootloader

    """
    type = None         # str indicating OS type
    kernel = None       # Kernel image
    initrd = None	# Initial Ram Disk
    root = None         # path to OS root
    appendline = None   # append line for lilo.conf

    @property
    def kernel_name(self):
        return os.path.split(self.kernel)[-1]

    @property
    def initrd_name(self):
	if self.initrd:
	    return os.path.split(self.initrd)[-1]
	return None

    @property
    def os_root(self):
	return self.root

    @property
    def _short_root(self):
	return os.path.split(self.root)[-1]

    @property 
    def tamu_kernel_path(self):
	return os.path.join("/boot/tamu/", "vmlinuz-%s"%  self._short_root)

    @property
    def tamu_initrd_path(self):
	rootpart = os.path.split(self.os_root)[-1]
	return os.path.join("/boot/tamu/","%s-%s"% ( self.initrd_name, self._short_root))

    @property
    def tamu_lilo_label(self):
	"""Return a lilo-friendly label for an OS on a foreign partition
	"""
	return "%s-%s"% (self.label, self._short_root)

    @classmethod
    def all(cls, include_running_os=True, op_data = None, bp_data = None,
            current_root = None):
        """Return one OperatingSystem instance for each OS
        installed
        Arguments:
            include_running_os:  True or False (self explanatory)
            op_data: os-prober data (for unittests only)
            bp_data: linux-boot-prober data (for unittests only)
            current_root: media.Partition object indicating the current partition mounted at /

        """
	if not op_data:
            op_data = sp.check_output(["os-prober", "&>/dev/null"])
        found_os = op_data.split("\n")
        
        systems = []
        if include_running_os and current_root is not None:
            myroot = current_root.path()
            LOG.debug("Creating Lilo boot entries for current OS")
            LOG.debug("Current root=%s"% myroot)
            if os.path.exists('/boot/vmlinuz'):
                current_os = cls()
                current_os.initrd = None
                current_os_tui = cls()
                current_os_tui.initrd = None
                current_os.kernel = '/boot/vmlinuz'
                current_os.label = 'Vector'
                current_os.type = 'linux'
                current_os.initrd = '/boot/initrd.gz'
                current_os_tui.initrd = '/boot/initrd.gz'
                current_os.root = myroot
                current_os_tui.label = 'Vector-TUI'
                current_os_tui.kernel = '/boot/vmlinuz'
                current_os_tui.root = myroot
                current_os_tui.type = "linux"
                current_os_tui.appendline = "2 verbose"
                systems.append(current_os)
                systems.append(current_os_tui)

        for i in found_os:
            if ":" not in i:
                continue
            root, long_desc, short_desc, os_type = \
                    [ s.strip() for s in i.split(":") ]
            operating_system = cls()
            operating_system.initrd = None
	    LOG.debug("Found %s on %s"% (os_type, root))
            if short_desc.startswith("Vector"):
                short_desc = "VectorLinux - %s"% root.split("/")[-1]
            if "vector" in long_desc.lower():
		if "64" in long_desc:
		    operating_system.label = "VLocity"
		else:
		    operating_system.label = "Vector"
            else:
                operating_system.label = short_desc
            if os_type.lower() == "linux":
                operating_system.type = "linux"
                bootinfo = get_linux_boot_info(root, bp_data)
                if bootinfo is None:
                    continue
                host_part = media.Partition(bootinfo["root"])
                operating_system.root = bootinfo["root"] #XX or root ??
#                operating_system.kernel = bootinfo["kernel"]
                mpoint = None
                needs_umount = False

                if not bootinfo["kernel"].strip().endswith("vmlinuz"):
                    if host_part.is_mounted():
                        mpoint= host_part.mountpoint
                    else:
                        mpoint = host_part.mount()
                        needs_umount = True
                    if os.path.exists(os.path.join(mpoint, "boot","vmlinuz")):
                        operating_system.kernel = "/boot/vmlinuz"
                    else:
                        operating_system.kernel = bootinfo["kernel"]
                # Check the initrd
                if bootinfo["initrd"]:
                    rdpath = mpoint + bootinfo["initrd"]
                    if os.path.exists(rdpath) and not os.path.isdir(rdpath):
                        operating_system.initrd = bootinfo["initrd"]
                    else:
                        operating_system.initrd = None
                        rds = ("boot/initrd","boot/initrd.gz")
                        for rd in rds:
                            if os.path.exists(os.path.join(mpoint, rd)) and \
                               not os.path.isdir(os.path.join(mpoint, rd)):
                                operating_system.initrd = os.path.join("/",rd)
                                break

                if needs_umount:
                    host_part.umount()                        
                    
                operating_system.initrd = bootinfo["initrd"]
                #XX: The bootinfo["append"] reports invalid stuff that may cause LiLO to fail
                #XX: Setting appendline to None until further notice.
                operating_system.appendline = None #: bootinfo["append"]
            elif "microsoft" in os_type.lower() or \
                    "windows" in os_type.lower() or \
		    "chain" in os_type.lower():
		        operating_system.long_desc = long_desc
			operating_system.short_desc = short_desc
                        operating_system.root = root
			if "microsoft" in short_desc.lower() or \
			   "windows" in short_desc.lower():
			    operating_system.label = "Windows-%s"% os.path.split(root)[-1]
			else:
			    operating_system.label = "Other_OS-%s"% os.path.split(root)[-1] 
                        operating_system.type = "chain"
            #XXX what about bsd, osx, etc.  Will 'chain' cover them?
            else:
                continue
            systems.append(operating_system)
	
	for unix in systems:
	    if unix.type.lower() == "linux":
		yield unix
	for other in systems:
	    if other.type.lower() != "linux":
		yield other

class MasterBootRecord(object):
    """The MBR of a device

    """
    def __init__(self, device):
        self.device = device # path to the device as in /dev/sda

    def read(self, bytes=2):
        """Read n bytes

        """
        with open(self.device) as f:
            data = f.read(bytes)
        return data

    def bootloader(self):
        """Return the bootloader installed in the MBR if any

        """
        bytes = self.read()
        if bytes == "\xEB\x63":
            return GRUB2
        elif bytes == "\xEB\x48":
            return GRUB
        elif bytes == "\xFA\xEB":
            return LILO
        else:
            return None


def get_linux_boot_info(root_partition, data = None):
    """Use linux-boot-prober to find out how to boot this OS

    /dev/sdb1:/dev/sdb1::/boot/vmlinuz26:/boot/kernel26.img:root=/dev/sdb1

    """
    if not data:
        data = sp.check_output(['linux-boot-prober', root_partition])
    else:
	data = "\n".join(
	    [ x for x in data.split("\n") if root_partition in x])
    hits = data.split("\n")
    if not hits:
        return None
    line = hits[0]
    if ":" not in line:
        return None
    line = line.strip()
    root, boot, _, kernel, initrd, append = \
            [ i.strip() for i in line.split(":") ]
    #XX: linux-boot-prober is reporting hits and misses for some of these
    #XX: boot parameters.  Lets verify that the kernel and initrd are correct
    p = media.Partition(root_partition)
    umount = False
    if not p.is_mounted():
        mountpoint = p.mount(p.mountpoint)
        umount = True
    else:
        mountpoint = p.mountpoint
    if not os.path.exists(mountpoint + initrd):
        LOG.debug("Reported initrd %s for OS in %s does not exist."% \
                  (mountpoint + initrd, root_partition))
        initrd = ""
        ramdisks = ("boot/initrd","boot/initrd.gz","initrd","initrd.img",
                    "boot/initrd.img")
        for rd in ramdisks:
            if os.path.exists(os.path.join(mountpoint, rd)):
                initrd = os.path.join("/",rd)
                LOG.debug("Located correct path to initrd for os on %s as %s"% \
                          (root_partition, initrd))
                break
    else:
        LOG.debug("Path to initrd for os on %s verified"% root_partition)

    if umount:
        p.umount()
        
    res = dict(root=root, kernel=kernel, initrd=initrd, append=append)
    return res

class FakeDeviceTestCase(unittest.TestCase):

    def setUp(self):
        (fd, self.path) = tempfile.mkstemp(prefix="fake-device-")
        f = os.fdopen(fd)
        f.seek(140000)
        os.write(fd, "0")
	self.op_data = "\n".join(
		("/dev/sda1:Windows 7 (loader):Windows:chain",
		"/dev/sda2:VLocity 7.1 B2:Linux:linux",
		"/dev/sdb1:VectorLinux 7.1 B1.1:Linux:linux",
		"/dev/sdb2:Slackware Linux 14.1:Linux:linux"))
	self.bp_data = "\n".join(
	    ("/dev/sda2:/dev/sda2::/boot/vmlinuz:/boot/initrd:root=/dev/sda2",
		"/dev/sdb1:/dev/sdb1::/boot/vmlinuz:/boot/initrd:root=/dev/sdb1",
		"/dev/sdb2:/dev/sdb2::/boot/vmlinuz:/boot/initrd:root=/dev/sdb2"))

    def tearDown(self):
        os.unlink(self.path)


class LiloTestCase(FakeDeviceTestCase):

    def test_lilo(self):
        if not os.path.exists('/sbin/lilo'):
            return #XXX 
        lilo = Lilo(self.path)
        for ops in OperatingSystem.all(include_running_os=True,
	    op_data = self.op_data, bp_data = self.bp_data):
            lilo.add_os(ops)
        lilo.write_config(config_path="/tmp/lilo-test")
	# Test to make sure the entries got written to the config file
	found = [ entry.root for entry in LiloDotConfEntry.all('/tmp/lilo-test')]
	written = [ x.split(':')[0] for x in self.op_data.split("\n")]
	for i in written:
	    self.assertIn(i, found)
#        lilo.test_install("/tmp/lilo-test", self.path)
#        mbr = MasterBootRecord(self.path)
#        self.assertEqual(mbr.bootloader(), LILO)


class Grub2TestCase(FakeDeviceTestCase):

    def test_grub2(self):
        return #XXX
        grub2 = Grub2(self.path, data = self.testdata)
        grub2.write_config(config_path="/tmp/grub2-test")
        grub2.test_install("/tmp/grub2-test", self.path)
        mbr = MasterBootRecord(self.path)
        self.assertEqual(mbr.bootloader(), GRUB2)


if __name__ == "__main__":
    unittest.main()
