# -*- coding: utf8 -*-

#    This file is part of vinstall.
#
#    vinstall is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vinstall is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with vinstall.  If not, see <http://www.gnu.org/licenses/>.


""" Convinience methods for working with partitions for the vectorlinux installer

"""

import utils
import parted
import unittest, tempfile, os
from vinstall.backend import media
from vinstall.core import log


__author__ = "Moises Henriquez"
__email__ = "moc.liamg@xnl.e0m"[::-1]
__version__ = "0.1"
__date__ = "2012-06-05"


LOG = log.get_logger(__name__)


class DiskPartitioner(object):
    """ Class used to modify the partition table on the given disk

    """
    def __init__(self, disk):
        """ Arguments:

                disk - a media.Disk object

        """
        self.disk = disk
        self.last_used_sector = 2048
        self.dirty = False
        if self.disk._disk:
            self.disk._disk.setFlag(1)
        LOG.debug("Partitioning %s" % disk)

    def has_partition_table(self):
        """Find if there is a partition table. PTs start at position 0x1BE and
        contains 4 entries of 16 bytes, one for each partition

        """
        with open(self.disk._device.path) as f:
            f.seek(0x1BE)
            data = f.read(16)
            if data == "\x00" * 16:
                LOG.debug("Disk has no partition table")
                return False
            else:
                LOG.debug("Disk has partition table")
                return True

    def create_partition_table(self, table_type="msdos"):
        """ Create the partition table on the disk.
        This is only necessary when using a fresh disk that has
        never been partitioned before.

        """
        LOG.debug("Creating partition table in %s" % self.disk._device)
        self.disk._disk = parted.freshDisk(self.disk._device, "msdos")
        self.disk._disk.has_partition_table = True
	self.dirty = False

    def delete_all_partitions(self):
        """ Delete all partitions from drive

        """
        LOG.debug("Deleting all partitions")
        #self.disk._disk.deleteAllPartitions()
        self.disk._disk = parted.freshDisk(self.disk._device, "msdos")
        self.disk._disk.has_partition_table = True

    def partitions_number(self):
        """Return the number of partitions in disk

        """
        return len(self.disk._disk.partitions)

    def add_partition(self,  size=0, units='MB'):
        """Add a partition to the disk.
        Args:  size = partition size
               units = partition units (MB, GB, MiB, GiB), (defaults to MiB)

        """
        LOG.debug("Adding partition of size %s%s" % (size, units))
        mypsize = parted.sizeToSectors(size, units, self.disk._device.sectorSize)
        # first partition starts at sector 1
        # find out where the free space is
        myconst = parted.Constraint(device = self.disk._device)
        for i in self.disk._disk.getFreeSpaceRegions():
            if i > mypsize:  # partition fits here
                if not self.dirty:
                    start_marker = max(i.start, 2048)
		    self.dirty = True
                else:
                    start_marker = self.last_used_sector + 1
                LOG.debug("Start: %s" % start_marker)
                break
        else:
            raise RuntimeError("There is no free space for partition")

        mygeom = parted.Geometry(device = self.disk._device,
                                 start = start_marker,
                                 length = mypsize)
        myfs = parted.FileSystem(type='ext2', geometry=mygeom)
        mypartition = parted.Partition(disk=self.disk._disk,
                                       fs = myfs,
                                       type=parted.PARTITION_NORMAL,
                                       geometry = mygeom)
        myconst = parted.Constraint(exactGeom = mygeom)
        self.last_used_sector = mypartition.geometry.end
        # Add the partition to the disk
        self.disk._disk.addPartition(partition = mypartition,
                                     constraint = myconst)

    def write_changes(self):
        """Finalize changes to the disk.  This needs to be called
        after creating partitions or deleting partitions to make sure
        the changes are actually written to the disk.

        """
        LOG.debug("Commiting changes")
        self.disk._disk.commit()


class DiskPartitionerTestCase(unittest.TestCase):

    def setUp(self):
        (fd, self.path) = tempfile.mkstemp(prefix="fake-device-")
        f = os.fdopen(fd)
        f.seek(2**32)
        os.write(fd, "0")

    def tearDown(self):
        os.unlink(self.path)

    def test_create_partition_table(self):
        device = parted.Device(self.path)
        disk = media.Disk()
        disk._device = device
        p = DiskPartitioner(disk)
        p.create_partition_table()
        p.add_partition(size=5)
        p.write_changes()
        self.assertTrue(p.has_partition_table())

    def test_add_partition(self):
        device = parted.Device(self.path)
        disk = media.Disk()
        disk._device = device
        p = DiskPartitioner(disk)
        p.create_partition_table()
        p.add_partition(size=5)
        p.write_changes()
        self.assertEqual(len(disk._disk.partitions), 1)

    def test_add_partition_twice(self):
        device = parted.Device(self.path)
        disk = media.Disk()
        disk._device = device
        p = DiskPartitioner(disk)
        p.create_partition_table()
        p.add_partition(size=2)
        p.add_partition(size=3)
        p.write_changes()
        self.assertEqual(len(disk._disk.partitions), 2)


    def test_has_partition_table(self):
        device = parted.Device(self.path)
        disk = media.Disk()
        disk._device = device
        p = DiskPartitioner(disk)
        self.assertFalse(p.has_partition_table())
        p.create_partition_table()
        p.add_partition(size=5)
        p.write_changes()
        self.assertTrue(p.has_partition_table())

    def test_delete_all_partitions(self):
        device = parted.Device(self.path)
        disk = media.Disk()
        disk._device = device
        p = DiskPartitioner(disk)
        p.create_partition_table()
        p.add_partition(size=3)
        p.add_partition(size=2)
        p.write_changes()
        self.assertEqual(len(disk._disk.partitions), 2)
        p.delete_all_partitions()
        p.write_changes()
        self.assertEqual(len(disk._disk.partitions), 0)


if __name__ == "__main__":
    unittest.main()
