# -*- coding: utf8 -*-


"""Mountpoints module

"""

import os


class MountPoints(object):

    DEFAULT_ROOT_FS = "ext3" # Default os to be used for /, unless changed by the user.
    MINIMUM_ROOT_SIZE = 4 # Size in GB

    def __init__(self, partitions):

        self.partitions = set(partitions)
        self.filesystems = set(["ext2", "ext3", "ext4", "xfs", "jfs", "swap"])
        #XX:  ^^ 'reiserfs' removed because it causes problems when setting
        #XX:  default skel setup on new user account.  See
        #XX:  https://bitbucket.org/VLCore/vl72/issues/102/useradd-m-doesnt-copy-skel-dotfiles-on
        self.mountpoints = set(["/", "home", "boot", "var", "tmp", "opt",
            "swap"])
        self.mapping = dict((i, [None, None]) for i in self.partitions)

        self.default_filesystem = self.DEFAULT_ROOT_FS
        #XX: ^^ For compatibility purposes only.

    def set_mount(self, partition, mountpoint):
        """Declare a mountpoint for a partition

        """
        self.mapping[partition][0] = mountpoint

    def clear_mount(self, partition):
        """Declare that a partition shouldnt be mounted anymore

        """
        self.mapping[partition][0] = None

    def set_filesystem(self, partition, fs):
        """Declare that a partition should be formatted with fs

        """
        self.mapping[partition][1] = fs

    def clear_filesystem(self, partition):
        """Declare that a partition sholdnt be formatted

        """
        self.mapping[partition][1] = None

    def available_partitions(self):
        """Return available partitions

        """
        return self.partitions - set(self.mapping.keys())

    def available_mountpoints(self):
        """Return available mountpoints

        """
        return self.mountpoints - set(i[0] for i in self.mapping.values())
