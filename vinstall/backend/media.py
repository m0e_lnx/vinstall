#!/bin/env python

"""Media handling. Implements objects providing methods
for mounting, umounting, formating and querying media.

"""

__author__ = "rbistolfi"


import re
import os
import parted
from vinstall.backend import sp
from utils import mountiso, mount, umount, is_mounted, format_partition
from _ped import DiskLabelException


DEV_DIR = '/dev'
CD_INFO = '/proc/sys/dev/cdrom/info'
PARTITIONS = '/proc/partitions'
ISO_MOUNTPOINT = "/tmp/ISOMNT"


class StorageDevice(object):
    """Base class for Storage Devices
    
    """
    def size(self, unit="GB"):
        """ Returns size, by default in GB, unit can be GB or MB

        """
        return NotImplemented

    def path(self):
        """Return the node path for this device
        
        """
        return NotImplemented

    def is_read_only(self):
        """Return True if the device is read-only
        
        """
        return NotImplemented

    @property
    def id(self):
        """Return the 'id' assigned by udev to this device"""

        defdir = "/dev/disk/by-id"
        if not self.path():
            raise NotImplemented
        
        mypath = os.path.split(self.path())[-1]
        for dev in os.listdir(defdir):
            linksto = os.path.split(os.readlink(
                os.path.join(defdir, dev)))[-1]
            if linksto == mypath:
                return dev
        return None
        
    @property
    def isUSB(self):
        """Return true if this object represents a USB storage device"""
        if not self.path():
            raise NotImplemented

        return self.id.startswith('usb-')

       
class MountMixin(object):
    """Mixin for devices that can be mounted
    
    """
    @property
    def mountpoint(self):
        """Return actual mountpoint if device is mounted. If its not mounted,
        create a mountpoint from the device path. Wrapped in a property for API
        compatibility.
        
        """
        if self.is_mounted():
            for line in open("/proc/mounts"):
                device, mountpoint, _ = line.split(" ", 2)
                if device == self.device_path:
                    break
            else:
                err = "Couldn't find mountpoint for %s" % self.device_path
                raise RuntimeError(err)
            return mountpoint
        else:
            mntpoint =  self.device_path.replace("/dev", "/mnt")
            if not os.path.exists(mntpoint):
                os.mkdir(mntpoint)
            return mntpoint
 
    def mount(self, mountpoint=None, filesystem="auto"):
        """Mount the media in the specified mountpoint.
        
        """
        if mountpoint is None:
            mountpoint = self.mountpoint
        return mount(self.device_path, mountpoint, filesystem=filesystem)

    def umount(self):
        """Umount the media.
        
        """
        return umount(self.mountpoint)

    def is_mounted(self):
        """Returns True if the media is mounted, False otherwise.
        
        """
        return is_mounted(self.device_path)


class FormatMixin(object):
    """Mixin for devices that can be formated
    
    """
    def query_filesystem(self):
        """Retrieve the current filesystem type from the system.

        """
        try:
            pfs = self._parted_partition.fileSystem.type
        except AttributeError:
            # partition has no format
            pfs = ""
        if "swap" in pfs:
            pfs = "swap"
        return pfs

    def format(self, filesystem):
        """Create a filesystem in this partition.

        """
        format_partition(self.device_path, filesystem)


class Partition(StorageDevice, MountMixin, FormatMixin):
    """A class representing a partition in a hard disk.

    """
    def __str__(self):
        if self.query_filesystem():
            filesystem = self.query_filesystem()
        else:
            filesystem = "Not formated"
        return '%s (%s %s)' % (self.device_path, self.size("GB"), filesystem)

    def __repr__(self):
        return '<Partition %s>' % self.device_path

    def __init__(self, device_path):
        """Initializes a Media object from the device path.
        
        """
        self.device_path = device_path

    def size(self, unit="GB"):
        """Return the partition size in the specified unit as a string.
        Defaults to GB

        """
        try:
            s = self._parted_partition.getSize(unit)
        except AttributeError:
            s = self._parted_partition.getLength(unit)
        return Size(s, unit) 

    def path(self):
        """Return the device path (only for interface compatibility with
        Disk)

        """
        return self.device_path

    @property
    def uuid(self):
        """Return the uuid assined by libblkid to this device"""
        data = sp.check_output(['blkid', self.path()])
        for line in data.splitlines():
            if line.startswith(self.path()):
                ldata = line.split()
                for section in ldata:
                    if section.startswith("UUID="):
                        key,val = section.split("=")
                        return val.replace("\"","")
        return None

    @property
    def uuid_old(self):
        """Return the 'uuid' assigned by udev to this device"""
        defdir = "/dev/disk/by-uuid"
        if not self.path():
            raise NotImplemented
        mypath = os.path.split(self.path())[-1]
        for dev in os.listdir(defdir):
            linksto = os.path.split(os.readlink(
                os.path.join(defdir, dev)))[-1]
            if linksto == mypath:
                return dev
        return None

    @classmethod
    def all(cls):
        """Return all the partitions in the system

        """
        disks = [ i for i in Disk.all() if i.has_partition_table() ]
        partitions = []
        for dsk in disks:
            partitions.extend(dsk._disk.partitions)

        for part in partitions:
            if part.type in (parted.PARTITION_NORMAL,
                             parted.PARTITION_LOGICAL,):
                p = cls(part.path)
                p._parted_partition = part
                yield p


class CDRom(StorageDevice, MountMixin):
    """API for operating on CDROM devices

    """
    def __init__(self, device_path):
        super(CDRom, self).__init__()
        self.device_path = device_path
        self.is_cdrom = True
        self.is_disk = False
        self.is_iso = False
        
    def is_read_only(self):
        return True
    
    def path(self):
        # api compat
        return self.device_path

    @classmethod
    def all(cls):
        devices = list_cdroms()
        for dev in devices:
            yield cls(dev)


class ISOFile(StorageDevice, MountMixin):
    """API for operations with ISO files.

    """
    def __repr__(self):
        return "<ISOFile %s>" % self.path()
        
    def __init__(self, path):
        super(ISOFile, self).__init__()
        self.device_path = path
        self.is_iso = True
        self.is_cdrom = False
        self.is_disk = False

    def path(self):
        # API compat
        return self.device_path

    def is_read_only(self):
        return True

    @property
    def mountpoint(self):
        # API compat
        return ISO_MOUNTPOINT

    def mount(self, mountpoint=None):
        """Mount ISO in a loop device

        """
        if mountpoint is None:
            mountpoint = self.mountpoint
        return mountiso(self.path(), mountpoint)

    @classmethod
    def all(cls, root_dir):
        for f in os.listdir(root_dir):
            path = os.path.join(root_dir, f)
            if os.path.isfile(path) and f.endswith(".iso"):
                yield cls(path)


class Disk(StorageDevice, MountMixin, FormatMixin):
    """Disk objects provide access to hard disk devices

    """
    def __init__(self, device_path):
        super(Disk, self).__init__()
        self.device_path = device_path
        self._device = None
        self._disk = None
        self.is_cdrom = False
        self.is_disk = True
        self.is_iso = False
        
    def __repr__(self):

        return "<Disk %s>" % self.path()

    def __str__(self):

        return "%s - %s (%s)" % (self.model(), self.size(), self.path())

    def model(self):
        """Return the string describing the device model"""
        return self._device.model

    def path(self):
        self.device_path = self._device.path
        return self._device.path

    def size(self, unit="GB"):
        """ Returns size, by default in GB, unit can be GB or MB

        """
        # fix parted api change
        try:
            s = self._device.getSize(unit)
        except AttributeError:
            s = self._device.getLength(unit)
        return Size(s, unit)

    def is_read_only(self):
        return False

    def has_partition_table(self):
        """Return True if the disk has a partition table.  False otherwise

        """
        return self._disk is not None

    def partitions(self):
        """Return iterable with partitions from this disk

        """
        for part in self._disk.partitions:
            if part.type in (parted.PARTITION_NORMAL,
                             parted.PARTITION_LOGICAL,):
                p = Partition(part.path)
                p._parted_partition = part
                yield p

    @classmethod
    def all(cls):
        devices = ( dev for dev in parted.getAllDevices() if dev.readOnly is False )
        if not devices:
            raise RuntimeError("No hard drives found")
        for dev in devices:
            disk = cls(dev.path)
            disk._device = dev
            try:
                disk._disk = parted.Disk(dev)
            except DiskLabelException, e:
                #print "%s Has no partion table. Cannot read partitions" % disk
                pass
            yield disk


class Size(object):
    """Size objects provide an API for handling storage device sizes.
    They can be part of arithmetic operations (either with other Size
    instances or with other numeric types) and can be compared (also
    with other Size instances or numeric types.) Size instances holding
    values in different units can be compared safely, but arithmetic
    operations require the same unit for both objects.
    While doing arihmetic, keep in mind that values are stored internally
    as floats by default.

    """
    units = {
        "B":   1,
        "KB":  1000,
        "MB":  1000**2,
        "GB":  1000**3,
        "KiB": 1024,
        "MiB": 1024**2,
        "GiB": 1024**3
    }

    n_type = float

    def __repr__(self):
        return "<Size %s %s>" % (self.n, self.unit)

    def __str__(self):
        return "%s %s" % (round(self.n, 2), self.unit)
        
    def __init__(self, n, unit="GB"):
        self.unit = unit
        self.n = self.n_type(n)

    def __add__(self, other):
        if isinstance(other, Size):
            if self.unit != other.unit:
                return NotImplemented
            n = other.n
        else:
            selfn = self.n
            n = other
            return self.__class__(selfn + n, self.unit)

    def __mul__(self, other):
        if isinstance(other, Size):
            if self.unit != other.unit:
                return NotImplemented
            n = other.n
        else:
            n = other
        return self.__class__(self.n * n, self.unit)

    def __div__(self, other):
        if isinstance(other, Size):
            if self.unit != other.unit:
                return NotImplemented
            n = other.n
        else:
            n = other
        return self.__class__(self.n / n, self.unit)
        
    def __lt__(self, other):
        if isinstance(other, Size):
            selfn = self.to("B").n
            n = other.to("B").n
            return selfn < n
        return self.n < other

    def __gt__(self, other):
        if isinstance(other, Size):
            selfn = self.to("B").n
            n = other.to("B").n
            return selfn > n
        return self.n > other

    def __eq__(self, other):
        if isinstance(other, Size):
            selfn = self.to("B").n
            n = other.to("B").n
            return selfn == n
        return self.n == other

    def __float__(self):
        return float(self.n)

    def __int__(self):
        return int(self.n)

    def to(self, unit):
        """Convert this value to another unit.
        Unit can be one of 'B', 'KB', 'MB', 'KiB', 'MiB' or 'GiB'

        """
        n = self.n * self.units[self.unit] / self.units[unit]
        return self.__class__(n, unit)


def list_cdroms(proc_info=CD_INFO):
    """Find CDROM devices based in the information stored in the proc tree.

    """
    with open(proc_info) as file_handler:
        cdroms = re.findall(r'([hs]d[a-z]|s[rg]\d*)', file_handler.read(), re.M)
    return ( os.path.join("/dev", i) for i in cdroms )


def list_swap():
    """Returns a list of swap devices.

    """
    swaps = []
    for i in Partition.all():
        data = sp.check_output(['blkid', i.path()])
        if "swap" in data:
            swaps.append(i.path())
    return swaps
