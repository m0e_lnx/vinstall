#!/bin/env python
# coding: utf8

#    This file is part of vinstall.
#
#    vinstall is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vinstall is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with vinstall.  If not, see <http://www.gnu.org/licenses/>.

__author__ = "Moises Henriquez"
__author_email__ = "moc.liamg@xnl.E0M"[::-1]

"""Python API to managing system services on vectorlinux"""
import os
import glob
import unittest

class Service(object):
    # Provide service names with default runlevels and start/kill order in tuples
    # ((names), "service description", default_runlevels, start/kill_order)
    SERVICES=(
        (("syslog","log"),"system and kernel loggers", range(2, 6), 10),
        (("pcmcia",), "", range(2,6), 11),
        (("hotplug","usb"), "", range(2,6), 12),
        (("kudzu",), "", range(2, 6), 13),
        (("alsasound","alsa"), "Advanced Linux Sound system", range(2, 6), 14),
        (("apm","apmd","acpid"), "Advanced Power Management", range(2, 6), 18),
        (("paranoid","paranoid_firewall"),"Filter and masquerade TCP/IP Networking", range(2, 6), 20),
        (("network",),"Initialize TCP/IP networking", range(2, 6), 21),
        (("umlnet","wlan","pppd"), "", range(2, 6), 22),
        (("firewall", "portsentry", "gshield"), "Protect network ports from outside attack", range(2, 6), 23),
        (("dhcpcd",), "DHCP server", range(2, 6), 24),
        (("bind","named","dnsmasq"), "Light DNS and DHCP server", range(2, 6), 25),
        (("rcp","portmap"), "network portmap, needed by NIS and NFS.", range(2, 6), 30),
        (("ypbind",), "Client for Network Information Service", range(2, 6), 31),
        (("netfs",), "Mount remote NFS/SAMBA directories", range(2, 6), 32),
        (("lisa",), "LAN Information Server", (4, 5), 33),
        (("printer","lpd","lp","cups"),"Linux printing system", range(2, 6), 39),
        (("hplip",), "HP Printing services", range(2, 6), 35),
        (("cron",), "periodic tasks scheduler", range(2, 6), 40),
        (("at",), "exact time tasks scheduler", range(2, 6), 41),
        (("quota",), "Limit disk usage for users", range(2, 6), 42),
        (("acc","accounting"), "", range(2, 6), 43),
        (("inetd","inet","xinetd","xinet"), "internet service super server", range(2, 6), 50),
        (("sshd","ssh","openssh"),"Secure shell daemon", range(2, 6), 51),
        (("nisd","nis","yp"),"Server of Network Information Services", (3, 5), 52),
        (("nfsd",),"Network File Server", (3, 5), 53),
        (("samba","smbd","nmbd"), "", range(2, 6), 54),
        (("database","mysql","postgres"), "SQL database server", range(2, 6), 55),
        (("xfsst",), "TrueType font server for X. Not needed for XFree 4.", (3, 5), 69),
        (("ftpd","proftp","proftpd"),"", (3, 5), 71),
        (("smptd","sendmail","postfix","gmail"), "mail server daemon", (3, 5), 72),
        (("imap","imapd"), "", (3, 5), 73),
        (("nntp","nntpd"), "", (3, 5), 74),
        (("identd","oidentd","midentd"), "ident server for IRC (chatting)", (3, 5), 79),
        (("httpd","apache"), "HTTP web server", (3, 5), 80),
        (("lampp","xampp"),"Combo Apache+MySQL+PHP+Postfix",(3, 5), 80),
        (("jboss",), "", (3, 5), 87),
        (("jsp","tomcat","jetty"),"",(3, 5), 88),
        (("gpm",),"console mouse daemon", range(2, 6), 91),
        )

    def __init__(self):
        """Manage system services on the specified root path"""
        self.root = None
        self.name = None
    
    @property
    def _data(self):
        ret = ""
        for line in self.SERVICES:
            if self.name in line[0]:
                return line
        
    @property
    def default_runlevels(self):
        """Return the default runlevels this service should be enabled for."""
        if self._data:
            return self._data[-2]
        return range(2,6)
     
    @property
    def start_order(self):
        """Return the start order value for this service.
        If the service is not in the whitelist, we leave this None
        so that .enable can assign one"""
        if self._data:
            return self._data[-1]
        return None
    
    @property
    def description(self):
        """Return the service description for this service.
        If one is not defined, or if the service is not in the
        whitelist, we return 'foo daemon'"""
        if self._data:
            return self._data[1].strip() or "%s daemon"% self.name
        return "%s daemon"% self.name
            
    @property
    def initd(self):
        """Return the path to the init.d directory in relation to the
        root property set in all"""
        return os.path.join(self.root, "etc","init.d")
        
    @property
    def rcd(self):
        """Return the absolute path to the rc.d directory in relation to the
        root property set in all"""
        return os.path.join(self.root, "etc","rc.d") 
    
    @property
    def script_path(self):
        """Return the absolute path to the service script"""
        return os.path.join(self.initd, self.name)
    
    @property
    def kill_order(self):
        """Find the kill order based on the start order"""
        return 99 - self.start_order
        
    def _find_start_order(self, runlevel):
        """Return the highest unused number in this runlevel between 50 and 99"""
        assert runlevel in range(2,6), "invalid runlevel"
        digits = []
        listing = os.listdir(os.path.join(self.root, 'etc', 'rc%s.d'% runlevel))
        for item in listing:
            digits.append(int(''.join(filter(lambda x: x.isdigit(), item))))
        digits.sort()
        
        for i in xrange(99,50, -1):
            if i not in digits:
                return i
        return 99 # if nothing else is available
   
        
    def enable(self, runlevel):
        """Enable this service for the specified runlevel"""
        assert runlevel in range(2,6), "Invalid runlevel"
        rcdir = os.path.join(self.root, 'etc', 'rc%s.d'%runlevel)
        # Set the service script executable
        os.chmod(self.script_path, 775)
        cwd = os.getcwd()
        
        # At this point, the old scripts check to see if runlevel is the current
        # runlevel, and start the service.
        # TODO: ^^
        
        # make sure we have a start_order
        if self.start_order:
            sorder = self.start_order
            korder = self.kill_order
        else:
            # find default order
            sorder = self._find_start_order(runlevel)
            korder = 99 - sorder
        start_link = "S%s%s"% (sorder, self.name)
        kill_link = "K%s%s"% (korder, self.name)
        
        # Just to be sure, try to delete these.
        for link in glob.glob(os.path.join(rcdir, "*%s"%self.name)):
            os.remove(link)
        
        # create the startup link in the right dir
        os.chdir(rcdir)
        os.symlink('../init.d/%s'% self.name, start_link)
        # os.symlink(self.script_path, start_link.replace(self.root, "/"))
        
        # create the kill link for the shutdown scripts
        os.symlink('../init.d/%s'% self.name, kill_link)
        #os.symlink(self.script_path, kill_link.replace(self.root, "/"))

        # Go back to cwd
        os.chdir(cwd)
        
        return  
        
        
    def disable(self, runlevel):
        """Disable this service for the specified runlevel"""
        assert runlevel in range(2, 6), "Invalid runlevel"
        script_path = os.path.join(self.initd, self.name)
        # FIXME:  Stop the service if it is running.
        
        rcdir = os.path.join(self.root, 'etc', "rc%s.d"% runlevel)
        for link in glob.glob(os.path.join(rcdir, "*%s"%self.name)):
            os.remove(link)
            
        return
    
    @classmethod
    def all(cls, root="/"):
        topdir, dirnames, filenames = os.walk(os.path.join(root, 'etc','init.d')).next()
        for script in filenames:
            service = cls()
            service.name = script
            service.root = root
            yield service



class Tests(unittest.TestCase):
    def setUp(self):
        self.services = Service()
        self.testservice = self.services.all().next()
        self.fakeservice = Service()
    
    def test_kill_order(self):
        """Test that the kill order for the service is always 99-start_order"""
        sv = self.testservice.start_order
        right = 99 - sv
        self.assertEqual(right, self.testservice.kill_order)
        
    def test_order_value(self):
        self.assertIsInstance(self.testservice.start_order, int)
        
    def test_returned_runlevels(self):
        self.assertIsInstance(self.testservice.default_runlevels, list)
    
    def test_invalid_runlevel_enable(self):
        """Check for request to enable service on invalid runlevel"""
        return self.assertRaises(AssertionError,
            self.testservice.enable, 6)
    
    def test_invalid_runlevel_disable(self):
        """Check for request to disable service on invalid runlevel"""
        return self.assertRaises(AssertionError,
            self.testservice.disable, 0)
    
    def test_known_service_definitions(self):
		"""New additions to the definitios follow the same convention"""
		for item in self.services.SERVICES:
			self.assertEqual(len(item), 4)
 
                
if __name__ == '__main__':
    unittest.main()
