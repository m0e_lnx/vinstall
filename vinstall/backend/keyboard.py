#-*- coding: utf-8 -*-


"""Keyboard utilities"""


from vinstall.backend import sp


def xkb():
    """Get available keyboard settings"""

    keyboards = {}
    with open("/usr/share/X11/xkb/rules/base.lst") as kbd_def:

        for line in kbd_def.readlines():

            if line.startswith("!"):
                token, section = line.split()
                section_elements = keyboards.setdefault(section, {})
            else:
                try:
                    line = line.strip()
                    name, description = [i.strip() for i in line.split(" ", 1)]
                    section_elements[name] = description
                except ValueError:
                    #blank line or something
                    continue

    return keyboards


def set_kb_layout(layout):
    """Set the keyboard layout.
    #TODO: add variants and options
    """
    if os.environ.get("DISPLAY", None):
        command = ["setxkbmap"]
    else:
        command = ["loadkeys"]
    command.append(layout)
    sp.call(command)

