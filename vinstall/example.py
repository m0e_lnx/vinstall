#-*- coding: utf-8 -*-

"""Example application

"""

from vinstall.core import Render, Controller, model
from vinstall.core.wizard import WizardApplication as Application
import os
import time


class State(Controller):
    """ Welcome screen"""
    def render(self):
        self.config["counter"] = 0
        msg = "Welcome to the vectorlinux install process.  This program will" \
        "guide you through the installation process"
        w = model.SimpleText(msg)
        return Render("Welcome", "To infinity and beyond!.", w)

    def next(self):
        return State1

    def previous(self):
        return None

    def process(self, *args):
        pass

    def command(self, *args):
        pass


class State1(Controller):
    """The first stage of the application."""

    def init(self):
        self.config["counter"] += 1

    def render(self):
        """Create a render object.

        """
        option = model.BoolOption("Check this for partitioning with cfdisk")
        ddown = model.DropdownOptionList()
        ddown.label = "A dropdown menu"
        ddown.add_option("Select this if foo")
        ddown.add_option("Or this if bar")
        return Render("Partitioning", "You can use the cfdisk partitioning" \
                " application for preparing your disk for installation.",
                option, ddown)

    def next(self):
        """Return the next stage of the application.

        """
        return State2

    def previous(self):
        """Return the previous state of the application, if it is None, the
        application will just quit.

        """
        return State

    def process(self, result, result_ddown):
        """Do something with the user input. The arguments passed to this
        method will be equal to the number of models passed to the Render
        object in the render method.

        """

    def command(self, *results):

        def p(x):
            time.sleep(float(x))

        for i in range(10):
            yield p, (i + 1,), "Using yield"


class State2(Controller):
    "The second page of our application. The render method needs to return a "\
    "Render object. This render object will pass the abstract form elements "\
    "to the proper view. The first argument is the title of the page, the "\
    "second is the intro text, and finally, you pass all your options, for "\
    "example:\n" \
    """
        def render(self):
            return ("Title", "Intro", opt1, opt2, opt3)
    \n"""\
    "Where opt* are instances of any class from the core.model module."


    def render(self):
        """Create a Render object with our options. You can append as many
        options as you want. the first argument to the render object is the
        window title, then the introductory text, and then the form elements.
        The model objects usually take the captio or label as first arg.

        """
        option = model.TextOption("A text input")
        option1 = model.BoolOption("Are you sure?")
        return Render("Another form", self.__doc__, option, option1)

    def next(self):
        """The next state, None for quiting.

        """
        return None

    def previous(self):
        """The previous state, if you need to do any clean up, do it here.

        """
        return State1

    def process(self, result, result1):
        """Process user input"""
        pass

    def command(self, *result):
        "Command 10"


if __name__ == "__main__":
    #Pass the first stage to the controller, after the view module name.
    application = Application(State, config={})
    application.set_view("urwid")
    application.run()
