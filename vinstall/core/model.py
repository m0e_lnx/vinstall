#-*- coding: utf-8 -*-


"""Classes representing option types."""


__author__ = "rbistolfi"


class TextOption(object):
    """User is expected to provide data in form of text."""

    def __init__(self, name, maxlen = 0, short_desc = None, help_text = None):
        """Initialization of a TextOption instance."""

        self.name = name
        self.maxlen = maxlen
        self.short_desc = short_desc
        self.help_text = help_text


class PasswordOption(TextOption):
    """A TextOption for passwords"""
    pass


class BoolOption(object):
    """User is expected to provide a boolean value."""

    def __init__(self, name, short_desc = None, help_text = None):
        """Initialization of a BoolOption instance."""

        self.name = name
        self.short_desc = short_desc
        self.help_text = help_text


class ExclusiveOptionList(object):
    """User is expected to select one option from a list."""

    def __init__(self, *options):
        """Initialization of an ExclusiveOptionList instance."""

        self.options = list(options)

    def __iter__(self):
        """Make this object iterable.

        """
        return iter(self.options)

    def append(self, option):
        """Append an option to the list.

        """
        self.options.append(option)

    def add_option(name=None, tooltip=None, help=None):
        """Add an option to the options list

        """
        self.append(dict(name=name, tooltip=tooltip, help=help))


class DropdownOptionList(object):
    """User is expected to select one option from a list."""

    def __init__(self, label=None, *options):
        """Initialization of an ExclusiveOptionList instance."""

        self.label = label
        self.options = list(options)

    def append(self, option):
        """Append an option to the list.

        """
        self.options.append(option)

    def add_option(self, option=None, tooltip=None, help=None):
        self.append(dict(option=option, tooltip=tooltip, help=help))


class NumericOption(object):
    """User is expected to provide a numeric value."""

    def __init__(self, name, minvalue = 0, maxvalue = 0, short_desc = None,
        help_text = None):
        """Initialization of a NumericOption instance."""

        self.name = name
        self.minvalue = minvalue
        self.maxvalue = maxvalue
        self.short_desc = short_desc
        self.help_text = help_text


class IPAddress(object):
    """Internet Protocol model."""

    def __init__(self, name, short_desc = None, help_text = None):

        self.name = name
        self.short_desc = short_desc
        self.help_text = help_text

class SimpleText(object):
    """informative text label.

    """

    process = False

    def __init__(self, text, short_desc=None, help_text=None):
        """SimpleText initialization.

        """
        self.text = text
        self.short_desc = short_desc
        self.help_text = help_text

