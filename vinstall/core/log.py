#-*- coding: utf-8 -*-

"""A logging facility for Vinstall.

"""


__author__ = "rbistolfi"
__all__ = ["get_logger"]


import logging


def get_logger(name, level=logging.DEBUG, filename="installer.log"):
    """Get a logger, used just to avoid duplication of these line.
    Call it with __main__ as argument.

    """
    logging.basicConfig(filename=filename, level=level)
    logger = logging.getLogger(name)
    return logger
