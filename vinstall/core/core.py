#-*- coding: utf-8 -*-


"""Utilities for declaring classes as views of certain model."""


from log import get_logger


LOG = get_logger(__name__)


class RegistryError(Exception):
    """Exception raised for registry errors.

    """
    pass


class ViewRegistry(object):
    """A registry mapping views and models

    """
    def __init__(self):
        super(ViewRegistry, self).__init__()
        self._dict = {}
        self._main_window = None

    def renders(self, model):
        """Returns a class decorator that can be used for registering a view
        against certain model.

        """
        def register_view(cls):
            """Add a View class to the Views/Models mapping"""

            # check if the model is already in the registry.
            if model in self._dict:
                LOG.error("View already registered. Raising RegistryError")
                raise RegistryError("A view is already registered for this "\
                        "model: %s" % self._dict[model])

            # this flag indicates if there are values entered by the user that
            # needs to be processed by controller classes. Defaults to True.
            _process = getattr(model, "process", True)
            cls._process = _process

            # add the mapping to the registry
            self._dict[model] = cls
            LOG.debug("Registering view %s for %s" % (cls, model))
            return cls

        return register_view

    def get_view(self, model):
        """Answer with a view registered for the given model"""

        view = self._dict.get(model)
        if not view:
            LOG.error("View not found. Raising RegistryError")
            raise RegistryError("There is no view registered for %s" % model)
        LOG.debug("Found view %s for %s" % (view, model))
        return view


    def main_window(self, cls):
        """Class decorator for registering a main window"""

        LOG.debug("Registering main window: %s" % cls)
        self._main_window = cls
        return cls

    def get_main_window(self):
        """Returns the main window for a given view"""

        LOG.debug("Using main window: %s" % self._main_window)
        return self._main_window


_registry = ViewRegistry()
renders = _registry.renders
get_view = _registry.get_view
main_window = _registry.main_window
get_main_window = _registry.get_main_window
