#-*- coding: utf-8 -*-

"""
########
Vinstall
########


Introduction
============

Vinstall is a Python framework for an installer application for GNU/Linux
operating systems, specially VectorLinux and Vlocity Linux.


User guide
==========

The Controller and Application objects
--------------------------------------

The installer is like a wizard application. It will present to the user a set
of pages or screens, containing some options used for controlling the process.
Each page of the installer is represented by a Controller class. The controller
class as a unit, will define what to show to the user, how to react to the user
input, and what are the next and previous stages of the application. So what we
are actually going to do, is to write one controller class for each page, and
then we will create an Application object, which will direct the whole process
for us.


The Controller interface
------------------------

There is an Interface (a set of methods) that any controller class needs to
implement:

*render():*
    The render method needs to return a Render object. Here you will setup your
    options, the title and introductory text for the screen. The Render object
    will be explained later.

*process(\*args):*
    This method will be used to process the user input after she activates the
    "next" button. Use this for actually doing something. It will be called
    using the values from the user as arguments.

*next():*
    Return the next controller **class** or ´´None´´. ´´None´´ indicates the
    end of the application.

*previous():*
    Return the previous controller **class** or ´´None´´. The first controller
    class would tipically return ´´None´´. Use this method for performing any
    clean up if necessary.

After defining one controller class for each page, we need an Application
object, which takes the name of the view as first argument and the first
controller class as second argument. Use the run method of the Application
instance for starting the application. The Application object will instantiate
the first controller and present it to the user, after that it will direct the
whole process for you.


The Render object
-----------------

The Render object is in charge of taking the options,find a view or rendering
class for each them, and then will return them to the application object for
presenting them to the user. The only thing you need to know about it is how to
create and instance:

    Render(<title>, <intro>, <\*options>)

It is very simple, the first two arguments are strings, and the following
arguments must be instances of any class from the core.model module. you can
pass as amny options as you need.


The model module
----------------

The model module contains classes representing options in an abstract way.
Tipically, the constructor for any model class will take a string as first
argument which will be used to create a caption or label for the option. The
rest of the arguments should be optional,and may vary. NumericOption can take a
**minvalue** or a **maxvalue** argument, and so on. A **help** keyword argument
is accepted in every class, and should be rendered later as a tooltip or
help text, depending on which UI toolkit is used. Check out the
**vinstall.core.model** module for more information, keep in mind that this one
is likely to change during the installer development.
Todo: add details for each model class and its constructors.


Framework development guide
===========================

This section will document stuff like how to add a new View, how to create
model classes, how to extend the code, etc.
**Todo**

"""


import core, backend, ui
