#-*- coding: utf-8 -*-


"""An Gtk based view."""

# pylint cant understand my relative imports
# pylint: disable-msg=E1101,W0406,W0404


from __future__ import absolute_import

import gtk, time
from vinstall.core import core, model, command
from vinstall.backend import mountpoints


gtk.gdk.threads_init()


class GtkView:
    """Base class for Gtk view."""

    # overload some methods from the interface that are not needed for this
    # view
    def show(self):
        """Toggle all the widgets visible.

        """

    def connect(self):
        """Connect signals.

        """
        pass


@core.main_window
class GtkMainWindow(object):
    """Top level widget for Gtk views.

    """
    def __init__(self):
        """View initialization.

        """
        self.title = ""
        self.intro = ""
        self.widgets = []

        self.window = gtk.Window()
        self.main_container = gtk.VBox()
        self.container = gtk.VBox()

        self.next_button = gtk.Button(gtk.STOCK_GO_FORWARD)
        self.back_button = gtk.Button(gtk.STOCK_GO_BACK)
        self.disconnect_next = None
        self.disconnect_back = None

        self.window.add(self.main_container)
	self.window.set_position(gtk.WIN_POS_CENTER_ALWAYS)

        self._header()
        self._body()
        self._buttons_row()
        self._footer()
        self._setup()

    def _header(self):
        """Create a header for the main window, with things like a generic
        title and tag line

        """
        icon = gtk.Image()
        #icon.set_from_icon_name("system-software-update", gtk.ICON_SIZE_DIALOG)
        icon.set_from_file("/usr/share/pixmaps/logo.png")
        s = ('<span foreground="black" size="25000"><b>VectorLinux</b></span>\n'
                '<span foreground="black" size="large">When choice matters</span>')
        title = gtk.Label(s)
        title.set_use_markup(True)
        title.set_justify(gtk.JUSTIFY_LEFT)
        title.set_alignment(0, 0.5)
        title.set_padding(5, 5)

        container = gtk.HBox()
        container.pack_start(icon, False, False, 5)
        container.pack_start(title, False, False, 5)
        eb = gtk.EventBox()

        eb.add(container)
        eb.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
        self.main_container.pack_start(eb, False, False, 5)

    def _body(self):
        """Create the window body, this is where the widgets provided by
        controller classes are shown

        """
        scrolledwin = gtk.ScrolledWindow()
        scrolledwin.add_with_viewport(self.container)
        scrolledwin.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.container.set_border_width(10)
        self.main_container.pack_start(scrolledwin, True, True, 10)

    def _buttons_row(self):
        """Create the next and back buttons

        """
        self.next_button.set_use_stock(True)
        self.back_button.set_use_stock(True)
        container = gtk.HBox()
        container.pack_start(self.back_button, False, False, 5)
        container.pack_start(self.next_button, False, False, 5)
        halign = gtk.Alignment(1, 0, 0, 0)
        halign.add(container)
        self.main_container.pack_end(halign, False, False, 5)
        #self.main_container.pack_end(gtk.HSeparator(), False, False, 5)

    def _footer(self):
        """Create a footer for the main window

        """

    def _setup(self):
        """Put all the elements together

        """
        self.container.set_homogeneous(False)
        self.window.set_default_size(700, 450)
        self.window.set_border_width(5)
        self.window.connect("destroy", lambda *x: self.stop())

    def update(self):
        """Update the frame with a new body after the next or previous button
        is clicked. Used by the controller classes to update the body of the
        window.

        """
        for oldwidget in self.container:
            self.container.remove(oldwidget)

        title = gtk.Label("<span size='large'><b>%s</b></span>" % self.title)
        title.set_alignment(0, 0.5)
        title.set_use_markup(True)
        intro = gtk.Label(self.intro)
        intro.set_alignment(0, 0.5)
        intro.set_line_wrap(True)

        def label_size_allocate(widget, rect):
            "Resize label when container size changes"
            widget.set_size_request(rect.width, -1)

        intro.connect("size-allocate", label_size_allocate)

        self.container.pack_start(title, False, False, 5)
        self.container.pack_start(intro, False, False, 5)

        for widget in self.widgets:
            self.container.pack_start(widget, False, False, 5)
            widget.show()
        self.container.show()
        self.window.show_all()

    def refresh(self):
        """This can be called from a long running process to request screen
        updates.

        """
        while gtk.events_pending():
            gtk.main_iteration(False)

    def add_next_callback(self, func, user_arg=None):
        """Set a callback for the next button "click" signal. This is used by
        the controller classes to process user input and setup the next state
        of the application.

        """
        handler = self.next_button.connect("clicked", func, user_arg)
        self.disconnect_next = lambda: self.next_button.disconnect(handler)

    def add_previous_callback(self, func, user_arg=None):
        """Set a callback for the previous button "click" signal. Used by the
        controller classes for jumping to a previous state.

        """
        handler = self.back_button.connect("clicked", func, user_arg)
        self.disconnect_back = lambda: self.back_button.disconnect(handler)

    def clear_callbacks(self):
        """Remove every callback from the buttons.

        """
        if self.disconnect_next and self.disconnect_back:
            self.disconnect_next()
            self.disconnect_back()

    def disable_buttons(self):
        """Disable input on the buttons, preventing user input while tasks are
        running

        """
        self.next_button.set_sensitive(False)
        self.back_button.set_sensitive(False)

    def enable_buttons(self):
        """Enable buttons

        """
        self.next_button.set_sensitive(True)
        self.back_button.set_sensitive(True)

    def set_next_button_label(self, text):
        """Set next button label

        """
        self.next_button.set_label(text)

    def set_previous_button_label(self, text):
        """Set previous button label

        """
        self.back_button.set_label(text)

    def alert(self, text):
        """Show a popup when tasks are complete

        """
        dialog = gtk.MessageDialog(parent=self.window,
                flags=0,
                type=gtk.MESSAGE_INFO,
                buttons=gtk.BUTTONS_OK,
                message_format=text)
        dialog.run()
        dialog.destroy()

    def run(self):
        """Start the main loop.

        """
        gtk.main()

    def stop(self):
        """Stop the main loop.

        """
        gtk.main_quit()


@core.renders(model.SimpleText)
class GtkTextLabel(GtkView):
    """ A simple text label for information purposes only.

    """
    def __init__(self, label):
        """GtkTextLabel initialization.

        """
        self.text = label.text
        self.label = None

    def accept(self):
        """Return a text widget.

        """
        return self.label

    def configure(self):
        """We dont need it in this simple widget.

        """
	def label_size_allocate(widget, rect):
            "Resize label when container size changes"
            widget.set_size_request(rect.width, -1)

        self.label = gtk.Label(self.text)
        self.label.set_use_markup(True)
        self.label.set_line_wrap(True)
        self.label.connect("size-allocate", label_size_allocate)

    def get_value(self):
        """This widget has no user input. Return None.

        """
        return None


@core.renders(model.TextOption)
class GtkTextInput(GtkView):
    """A text input widget for rendering the TextOption model.

    """
    def __init__(self, text_option):

        # Expose the model
        self.option_name = text_option.name
        self.option_maxlen = text_option.maxlen
        self.option_short_desc = text_option.short_desc
        self.option_help = text_option.help_text

    def configure(self):
        """Setup the elements composing the model view."""

        self.label = gtk.Label(self.option_name)
        self.label.set_alignment(1, 0.5)
        self.edit = gtk.Entry()
        self.container = gtk.Table(1, 5, True)
        self.container.set_col_spacings(10)
        self.container.attach(self.label, 0, 1, 0, 1)
        self.container.attach(self.edit, 1, 3, 0, 1)

    def get_value(self):
        """Returns the text entered by the user."""

        return self.edit.get_text()

    def accept(self):
        """Returns the configured top level container widget."""

        return self.container


@core.renders(model.PasswordOption)
class GtkPasswordInput(GtkTextInput):
    """A text input that disables buttons until it has something"""

    def configure(self):
        """Setup events for text widget"""

        GtkTextInput.configure(self)
        self.window.next_button.set_sensitive(False)

        def password_is_valid(s):
            return len(s) >= 6

        def activate_buttons_if_valid(widget, *args, **kwargs):
            """Activate window buttons if password is valid"""
            s = self.get_value()
            if password_is_valid(s):
                self.window.next_button.set_sensitive(True)
            else:
                self.window.next_button.set_sensitive(False)

        self.edit.connect("changed", activate_buttons_if_valid)


@core.renders(model.NumericOption)
class GtkNumericInput(GtkView):
    """A widget accepting numeric input for rendering the NumericOption model.

    """
    def __init__(self, text_option):

        # Expose the model
        self.option_name = text_option.name
        self.option_short_desc = text_option.short_desc
        self.option_help = text_option.help_text

    def configure(self):
        """Setup the elements composing the model view."""

        # Widgets
        self.label = gtk.Label(self.option_name)
        self.edit = gtk.SpinButton()
        self.container = gtk.HBox()
        self.container.pack_start(self.label)
        self.container.pack_start(self.edit)

    def get_value(self):
        """Returns the text entered by the user."""

        return self.edit.get_value()

    def accept(self):
        """Returns the configured top level container widget."""

        return self.container


@core.renders(model.BoolOption)
class GtkBoolInput(GtkView):
    """A a two-states widget for boolean options.

    """
    def __init__(self, bool_option):

        self.option_name = bool_option.name
        self.option_short_desc = bool_option.short_desc
        self.option_help = bool_option.help_text

    def configure(self):
        """Set up the container widget.

        """
        self.check_box = gtk.CheckButton(label=self.option_name)

    def get_value(self):
        """Return the user input.

        """
        return self.check_box.get_active()

    def accept(self):
        """Return the toplevel widget.

        """
        return self.check_box


@core.renders(model.ExclusiveOptionList)
class GtkExclusiveList(GtkView):
    """A list of options, from which the user can pick only one.

    """
    def __init__(self, options_list):

        self.options_list = options_list.options
        self.selected_item = 0

    def configure(self):
        """Setup widgets.

        """
        self.widgets = []
        self.container = gtk.VBox()
        first_radio = gtk.RadioButton(label=self.options_list[0]["name"])
        first_radio.connect("toggled", self._on_radio_toggled, 0)
        first_radio.set_active(True)
        self.container.pack_start(first_radio)
        for index, option in enumerate(self.options_list[1:]):
            radio_button = gtk.RadioButton(group=first_radio,
                    label=option["name"])
            radio_button.connect("toggled", self._on_radio_toggled, index + 1)
            self.container.pack_start(radio_button)
            self.widgets.append(radio_button)

    def get_value(self):
        """Return the user input.

        """
        return self.selected_item

    def accept(self):
        """Return the top level widget.

        """
        return self.container

    def _on_radio_toggled(self, widget, data=None):
        """Set the self.selected_item attribute"""

        if widget.get_active():
            self.selected_item = data


@core.renders(model.DropdownOptionList)
class GtkDropdownList(GtkView):
    """A list of options, from which the user can pick only one.

    """
    def __init__(self, options):

        self.text = options.label
        self.options = options.options
        self.selected_item = 0

    def configure(self):
        """Setup widgets.

        """
        self.label = gtk.Label()
        self.label.set_alignment(1, 0.5)
	self.label.set_text(self.text)
        self.widget = gtk.combo_box_new_text()
        for d in self.options:
            self.widget.append_text(d["option"])
        self.widget.set_active(0)
        self.container = gtk.Table(1, 5, True)
        self.container.set_col_spacings(10)
        self.container.attach(self.label, 0, 1, 0, 1)
        self.container.attach(self.widget, 1, 3, 0, 1)
        self.container.show_all()

    def get_value(self):
        """Return the user input.

        """
        return self.widget.get_active()

    def accept(self):
        """Return the top level widget.

        """
        return self.container


@core.renders(model.IPAddress)
class GtkIPAddress(GtkView):
    """A widget designed for entering an IP address.

    """
    def __init__(self, ip_option):

        self.option_name = ip_option.name
        self.short_desc = ip_option.short_desc
        self.help = ip_option.help_text

    def configure(self):
        """Setup the input widgets and a container.

        """
        self.label = gtk.Label(self.option_name)
        self.container = gtk.HBox()
        self.container.pack_start(self.label)
        self.widgets = []
        for i in xrange(4):
            entry = gtk.Entry(max=3)
            self.container.pack_start(entry)
            self.widgets.append(entry)

    def get_value(self):
        """Return user input.

        """
        values = [ i.get_value() for i in self.widgets ]
        values = [ str(value) for value in values ]
        return ".".join(values)

    def accept(self):
        """Return the top level widget.

        """
        return self.container


@core.renders(command.CommandExecutor)
class GtkCommandExecutorView(GtkView):
    """Renders a Batch Processing class in a progressbar

    """
    def __init__(self, executor):

        executor.delegate = self
        self.executor = executor
        self.finished = False
        self.running = False

    def configure(self):
        """Setup widgets

        """
        self.caption = gtk.Label()
        self.progress_bar = gtk.ProgressBar()
        self.container = gtk.VBox()
        self.container.pack_start(self.caption)
        self.container.pack_start(self.progress_bar)
        self.container.set_border_width(40)

    def get_value(self):
        """Return user input. We return None since this widget does not accept focus

        """
        return None

    def on_execution_started(self, executor):
        """Called before starting to process the input queue

        """
        gtk.gdk.threads_enter()
        self.running = True
        self.progress_bar.set_fraction(0.01)
        gtk.gdk.threads_leave()

    def on_execution_ended(self, executor):
        """Called when the input queue is done

        """
        gtk.gdk.threads_enter()
        self.running = False
        self.finished = True
        self.progress_bar.set_fraction(1)
        gtk.gdk.threads_leave()

    def on_command_start(self, item):
        """Called before processing the item

        """
        gtk.gdk.threads_enter()
        self.caption.set_text(item.description)
        gtk.gdk.threads_leave()

    def on_command_end(self, command, total, done):
        """Called after item is complete. Args is a tuple with
        item, result of the process, items completed and total items

        """
        gtk.gdk.threads_enter()
        total = float(total)
        self.progress_bar.set_fraction(done / total)
        gtk.gdk.threads_leave()

    def accept(self):
        """Return the top level widget.

        """
        return self.container


@core.renders(mountpoints.MountPoints)
class MountPointSelection(GtkView):
    """Interface for declaring mountpoints.

    """
    DNF = "Do not format"
    DNU = "Not use"

    def __init__(self, mountpoints):

        self.mountpoints = mountpoints
        self.partitions = sorted(mountpoints.partitions, key=lambda x: x.path())
        self.columns = "Device", "Mountpoint", "Filesystem"
        self.str_to_partition_map = dict((str(i), i) for i in
                self.mountpoints.partitions)
        self.window.next_button.set_sensitive(False)
        self.window.back_button.connect("clicked",
                lambda *_: self.window.next_button.set_sensitive(True))
        self.root_selected = False

    def configure(self):
        """Setup the elements composing the model view."""

        self.store = gtk.ListStore(str, str, str)
        self.treeview = gtk.TreeView(self.store)
        self.treeview.connect("realize", self.on_mouse_over)
        for i, c in enumerate(self.columns):
            cell = gtk.CellRendererText()
            col = gtk.TreeViewColumn(c, cell, text=i)
            self.treeview.append_column(col)
        self.show_partitions()
        self.treeview.connect("button-press-event", self.on_click)
        self.treeview.set_property('hover-selection', True)
        self.treeview.show_all()

    def show_partitions(self):
        for p in self.partitions:
            self.store.append([p, self.DNU, self.DNF])

    def on_click(self, treeview, event):
        """Answer to click on treeview
        If Mountpoint column is clicked then show the mounpoints menu
        If filesystem column is clicked, show filesystem menu

        """
        if event.button:
            x = int(event.x)
            y = int(event.y)
            path_info = treeview.get_path_at_pos(x, y)
            if path_info is not None:
                path, col, cellx, celly = path_info
                treeview.grab_focus()
                treeview.set_cursor(path, col, 0)
                index = treeview.get_columns().index(col)
                if index in (0, 1):
                    # Mountpoint column clicked
                    self.show_mountpoints_menu(path[0], event)
                elif index == 2:
                    # Format column clicked
                    self.show_filesystem_menu(path[0], event)
                else:
                    pass
            return True

    def verify_mountpoint_selection(self, menu, row, event):
        """Processed after the mountpoint has been selected.
        Provides methods select a default fs for / and swap.

        """
        mpoint = menu.get_active().get_label()
        partition = self.store[row][0]
        if mpoint is "/":
            self.window.next_button.set_sensitive(True)
            self.filesystem_selected(row, partition,
                self.mountpoints.DEFAULT_ROOT_FS)
        else:
            if "swap" in mpoint:
                self.filesystem_selected(row, partition, "swap")
        partitions = [ i[0] for i in self.mountpoints.mapping.values() ]
        if "/" not in partitions:
            self.window.next_button.set_sensitive(False)

    def _read_partition_size(self, partition):
        """Return strip the partition size from the provided string.
        partition should be a media.Partition object

        """
        return partition.size()

    def show_mountpoints_menu(self, row, event):
        """Show mountpoints menu

        """
        menu = gtk.Menu()
        partition = self.store[row][0]
        dnu = gtk.MenuItem(self.DNU)
        dnu.connect_object("activate", self.mountpoint_selected, row,
                partition, None)
        dnu.show()
        menu.append(dnu)

        psize = self._read_partition_size(self.str_to_partition_map[partition])

        for i in self.mountpoints.available_mountpoints():
            item = gtk.MenuItem(i)
            item.connect_object("activate", self.mountpoint_selected,
                    row, partition, i)
            if i == "/" and psize < self.mountpoints.MINIMUM_ROOT_SIZE:
                item.set_property('sensitive', False)
                item.set_property('label', '/ (%sGB Min.)'% str(self.mountpoints.MINIMUM_ROOT_SIZE))
            item.show()
            menu.append(item)
        menu.connect("selection-done", self.verify_mountpoint_selection, row, event)
        menu.show()
        menu.popup(None, None, None, event.button, event.time)

    def show_filesystem_menu(self, row, event):
        """Show a menu with filesystem options

        """
        menu = gtk.Menu()
        partition = self.store[row][0]
        dnf = gtk.MenuItem(self.DNF)
        dnf.connect_object("activate", self.filesystem_selected, row,
                partition, None)
        dnf.show()
        menu.append(dnf)
        for i in self.mountpoints.filesystems:
            item = gtk.MenuItem(i)
            item.connect_object("activate", self.filesystem_selected,
                    row, partition, i)
            item.show()
            menu.append(item)
        menu.show()
        menu.popup(None, None, None, event.button, event.time)

    def mountpoint_selected(self, row, partition, mountpoint):
        """A mountpoint has been selected in the mountpoints menu

        """
        partition = self.str_to_partition_map[partition]
        if not mountpoint:
            self.mountpoints.clear_mount(partition)
            value = self.DNU
        else:
            self.mountpoints.set_mount(partition, mountpoint)
            value = mountpoint
        self.store[row][1] = value

    def filesystem_selected(self, row, partition, filesystem):
        """A mountpoint has been selected in the mountpoints menu

        """
        partition = self.str_to_partition_map[partition]
        if not filesystem:
            self.mountpoints.clear_filesystem(partition)
            value = self.DNF
        else:
            self.mountpoints.set_filesystem(partition, filesystem)
            value = filesystem
        self.store[row][2] = value

    def on_mouse_over(self, widget):
        """Change mouse pointer on mouse over"""
        pointer = gtk.gdk.Cursor(gtk.gdk.HAND2)
        widget.window.set_cursor(pointer)

    def get_value(self):
        """Returns the text entered by the user."""
        return self.mountpoints.mapping

    def accept(self):
        """Returns the configured top level container widget."""

        return self.treeview
