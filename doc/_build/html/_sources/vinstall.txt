vinstall Package
================

:mod:`vinstall` Package
-----------------------

.. automodule:: vinstall.__init__
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`example` Module
---------------------

.. automodule:: vinstall.example
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    vinstall.backend
    vinstall.core
    vinstall.extensions
    vinstall.ui

